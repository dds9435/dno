extends Node2D

func _ready():
	randomize()
	var a = $Sprite.texture.get_data()
	a.lock()
	for i in range(0,32):
		for j in range(0,32):
			if randi()%8 == 0:
				a.set_pixel(i,j,Color(0,0,0,0))
	a.unlock()
	var b = ImageTexture.new()
	b.create_from_image(a,0)
	$Sprite.texture = b