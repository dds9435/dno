extends Control

var template = "[table=4][cell][color=#999999]nickname                   [/color][/cell][cell][color=#999999]K[/color][/cell][cell][color=#999999]D[/color][/cell][cell][color=#999999]C[/color][/cell]"
var red = {}
var blue = {}

var hidden = true

func _process(delta):
	if Input.is_action_just_pressed("tab"):
		if hidden:
			$anim.play("show")
			hidden = false
	if Input.is_action_just_released("tab"):
		if !hidden:
			$anim.play("hide")
			hidden = true

class csorter:
	static func sort(a, b):
		if a[0] < b[0]:
			return true
		return false

func update_tab_menu():
	$"panel/red".bbcode_text = template
	$"panel/blue".bbcode_text = template
	red = {}
	blue = {}
	
	var stats = {}
	
	for i in networkv2.players.keys():
		stats[[i,networkv2.get_player_text_data(i)]] = [i, networkv2.players[i]["stats"]]
	stats[[networkv2.info["id"], networkv2.get_player_text_data(networkv2.info["id"])]] = [networkv2.info["id"], $"/root/game".stats]
	
	for i in stats.keys():
		var stat = stats[i][1]
		var id = stats[i][0]
		var score = stat[0] + stat[2] - stat[1]
		var row = "[cell][color="
		if id in $"/root/game".in_game:
			row += i[1][1]
		else:
			match i[1][1]:
				"#ff9999": row += "#886666"
				"#9999ff": row += "#666688"
		row+="]"+i[1][0] + "[/color][/cell]"
		row+="[cell]" + str(stat[0]) + "[/cell]" + "[cell]" + str(stat[1]) + "[/cell]" + "[cell]" + str(stat[2]) + "[/cell]"
		
		match i[1][1]:
			"#ff9999": red[[score,stats[i][0]]] = row
			"#9999ff": blue[[score,stats[i][0]]] = row
		
	
	var red_sorted = red.keys()
	red_sorted.sort_custom(csorter, "sort")
	red_sorted.invert()
	var blue_sorted = blue.keys()
	blue_sorted.sort_custom(csorter, "sort")
	blue_sorted.invert()
	
	for i in red_sorted:
		$"panel/red".append_bbcode(red[i])
	for i in blue_sorted:
		$"panel/blue".append_bbcode(blue[i])