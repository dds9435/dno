extends Node2D

var team = -1
var state = -1
var crouch = false

func _ready():
	u()

func u():
	update()

func _draw():
	if crouch:
		return
	if team > -1:
		match team:
			0: draw_rect(Rect2(-1,-1,3,3),Color(1,0.3,0.3))
			1: draw_rect(Rect2(-1,-1,3,3),Color(0.5,0.5,1))
	elif state > -1:
		match state:
			1: draw_circle(Vector2(0,0),2,Color(0.7,0.7,0.7))
			2: draw_circle(Vector2(0,0),2,Color(1,0,0))
			3: draw_circle(Vector2(0,0),2,Color(0.3,0.3,1))