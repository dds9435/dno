extends "res://assets/scripts/net_constants.gd"

var port = DEFAULT_PORT
var tcp_server
var enet_unreliable

var connection = Dictionary()
var unreliable_id = Dictionary()

var game_size = 0
var game_stage = 0

var max_clients = 16

# progress, state, cap_team, capturers, pos, cap_time
var points = []
# progress, state, cap_team, capturers (count)
var prev_points = [
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	}
]

var teams = [
	[],
	[]
]

var respawn_timers = {
}
var in_game = []

var prev_game_timer = 10
var prev_players_wait_timer = 10
var game_timer = MATCH_TIME
var game_timer_stop = true
var win_condition = ""
var players_wait_timer = WAIT_TIME

var feed = []
var team_points_max = 0
var team_points = [
	0, # red
	0, # blu
	0, # red_b
	0  # blu_b
]

var error_log = {}
var alive_log = {}

class client_data:
	var peer = PacketPeerStream.new()
	var info = Dictionary()
	var player = Dictionary()
	var respawn_time = 3.0
	var seat = null
	var id = -1


func _ready():
	state.print_log("Generating map...")
	set_process(false)
	randomize()
	
	$map.generate_map()
	
	yield($map,"generation_end")
	
	# DEFAULT_POINTS - array, i - dict
	for i in DEFAULT_POINTS:
		points.append(i.duplicate())

	# points - array, i - counter
	for i in points.size():
		points[i]["pos"] = $map.map_to_world($map.key_points[i])
	
	tcp_server = TCP_Server.new()
	enet_unreliable = NetworkedMultiplayerENet.new()
	
	get_tree().multiplayer.connect("network_peer_packet", self, "unreliable_packet")
	
	if tcp_server.listen(port) == OK and enet_unreliable.create_server(port, max_clients) == 0:
		state.print_log("Server started on port " + str(port))
		get_tree().set_network_peer(enet_unreliable)
		set_process(true)


func _process(delta):
	
	for i in error_log:
		if unreliable_id.has(i):
			if error_log[i] > 20:
				for j in connection.keys():
					if connection[j] == unreliable_id[i]:
						state.print_log("someone is full of wrong packets!")
						broadcast_disconnect(j)
						error_log.erase(i)
		else:
			error_log.erase(i)
	
	for i in alive_log:
		if connection.has(i):
			alive_log[i] += delta
			if alive_log[i] > ALIVE_TIMER_TIMEOUT:
				state.print_log("someone died!")
				alive_log.erase(i)
				broadcast_disconnect(i)
		else:
			alive_log.erase(i)
	
	if tcp_server.is_connection_available() and connection.size() < max_clients:
		var client = tcp_server.take_connection()
		connection[client] = client_data.new()
		connection[client].player = DEFAULT_PLAYER.duplicate(true)
		connection[client].peer.set_stream_peer(client)
		state.print_log("Someone connecting...")
		
	for client in connection:
		
		if !client.is_connected_to_host():
			state.print_log(connection[client].info["nickname"] + " connection interrupted")
			
			broadcast_disconnect(client)
			
			continue
			
		if connection[client].peer.get_available_packet_count() > 0:
			
			for i in range(connection[client].peer.get_available_packet_count()):
				var data = connection[client].peer.get_var()
				
				match data[0]:
					
					PLAYER_CONNECT:
						
						connection[client].info = data[1]
						connection[client].id = connection[client].info["id"]
						broadcast_connect(client)
						send_info(client)
						send_map(client)
						send_players(client)
						send_points(client)
						assign_team(client)
						update_game_size()
						broadcast_game_timer("stop")
						broadcast_stats(connection[client].id, connection[client].player["stats"])
						send_stats(client)
						respawn_timers[client] = connection[client].respawn_time
						state.print_log(connection[client].info["nickname"] + " connected")
						var nicknames = ""
						for c in connection:
							nicknames+=connection[c].info["nickname"] + ", "
						state.print_log("Currently on server: " + nicknames.left(nicknames.length()-2))
						
					PLAYER_DATA:
						
						match data[1]:
								
							"умер мужик":
								
								broadcast_umer_muzhik(client)
								var s = connection[client].player["stats"]
								connection[client].player = DEFAULT_PLAYER.duplicate(true)
								connection[client].player["stats"] = s
								broadcast_stats(connection[client].id, connection[client].player["stats"])
								assign_team(client)
								respawn_timers[client] = connection[client].respawn_time
								broadcast_player(client)
								
								update_feed(["kill", str(connection[client].id), data[2]])
								
							"playing":
								
								var seat
								var p = points[1 - connection[client].player["team"]]
								if p["seats"].has(connection[client].seat):
									p["seats"].erase(connection[client].seat)
								while true:
									seat = randi() % 8
									if !seat in p["seats"]:
										break
								p["seats"].append(seat)
								connection[client].seat = seat
								connection[client].peer.put_var([PLAYER_DATA, "spawn_pos", p["pos"] + Vector2(-336 + 96*seat, 0)]) # MAGIC
								
								if connection[client].player.has(data[1]):
									connection[client].player[data[1]] = data[2]
									broadcast_player_data(client,data)
								
								update_feed(["res", str(connection[client].id)])
								
							_:
								
								if connection[client].player.has(data[1]):
									connection[client].player[data[1]] = data[2]
									broadcast_player_data(client, data)
								else:
									state.print_log("Wrong packet from " + connection[client].info["nickname"] + "!")
								
					HIT:
						
						if !data[4] in ["root", ""]:
							if str(data[1]) == data[3]:
								if unreliable_id[int(data[4])].player["pos"].distance_to(data[6]) < 42:
									if unreliable_id[int(data[4])].player["hitbox"].has(data[5]) and unreliable_id[int(data[3])].player["weapon_id"] != null:
										unreliable_id[int(data[4])].player["hitbox"][data[5]] -= data[7]
									
										for c in connection:
											connection[c].peer.put_var(
													[PLAYER_DATA, "hitbox", data[4], data[5], unreliable_id[int(data[4])].player["hitbox"][data[5]], data[3]]
											)
										
										update_feed(["hit", data[3], data[4], data[5], data[7], unreliable_id[int(data[4])].player["hitbox"][data[5]]])
						
					MESSAGE:
						
						var text = data[1]
						var team = data[2]
						
						var team_formated = " to all: "
						var nickname = connection[client].info["nickname"]
						var id = connection[client].id
						
						if team:
							if client in teams[TEAM_RED]:
								for c in teams[TEAM_RED]:
									connection[c].peer.put_var([3, text, id, team])
									
								team_formated = " to red team: "
								
							elif client in teams[TEAM_BLU]:
								for c in teams[TEAM_BLU]:
									connection[c].peer.put_var([3, text, id, team])
								
								team_formated = " to blue team: "
								
						else:
							for c in connection:
								connection[c].peer.put_var([3, text, id, team])
						
						state.print_log("Message from " + nickname + team_formated + text)
					
					ALIVE:
						
						alive_log[client] = 0.0
						
					EXIT:
						
						if connection.has(client): 
							broadcast_disconnect(client)
							return
		# points
		
		for i in points:
			if connection[client].player["pos"].distance_to(i["pos"]) < 256:
				if !i["capturers"].has(connection[client]):
					i["capturers"].append(connection[client])
			else:
				if i["capturers"].has(connection[client]):
					i["capturers"].erase(connection[client])
	
		if connection[client].player["pos"].y > 3600:
			connection[client].peer.put_var([PLAYER_DATA, "hitbox", str(connection[client].id), "c", 0, "void"])
		
	for i in points:
		var cap_team = -1
		var idx = points.find(i)
		
		for j in i["capturers"]:
			match j.player["team"]:
				TEAM_RED:
					if cap_team != TEAM_BLU:
						cap_team = TEAM_RED
					else:
						cap_team = -1
				TEAM_BLU:
					if cap_team != TEAM_RED:
						cap_team = TEAM_BLU
					else:
						cap_team = -1
		
		i["cap_team"] = cap_team
		
		if cap_team > -1 and connection.size() > 1 and players_wait_timer < 0:
			if i["progress"] < i["cap_time"] and i["state"] - 2 != cap_team and i["state"] > 0:
				i["progress"] += delta * i["capturers"].size()
			else:
				if i["progress"] != 0.0 and i["state"] != cap_team + 2:
					update_feed(["cap", idx, cap_team, i["capturers"]])
					i["progress"] = 0.0
					i["state"] = cap_team + 2
			
		if i["capturers"].size() == 0:
			if i["progress"] > 0:
				i["progress"] -= delta
			else:
				i["progress"] = 0.0
		
		# network
		
		if i["progress"] != prev_points[idx]["progress"]:
			
			broadcast_point(idx, "progress", i["progress"] / i["cap_time"])
			prev_points[idx]["progress"] = i["progress"]
		
		if i["state"] != prev_points[idx]["state"]:
			
			broadcast_point(idx, "state", i["state"])
			prev_points[idx]["state"] = i["state"]
		
		if i["cap_team"] != prev_points[idx]["cap_team"]:
			
			broadcast_point(idx, "cap_team", i["cap_team"])
			prev_points[idx]["cap_team"] = i["cap_team"]
		
		if i["capturers"].size() != prev_points[idx]["capturers"]:
			
			broadcast_point(idx, "capturers", i["capturers"].size())
			prev_points[idx]["capturers"] = i["capturers"].size()
	
	# respawn_timer
	
	for i in respawn_timers.keys():
		respawn_timers[i] -= delta
		connection[i].peer.put_var([PLAYER_DATA, "respawn_timer", respawn_timers[i]])
		if respawn_timers[i] < 0:
			connection[i].peer.put_var([PLAYER_DATA, "respawn"])
			respawn_timers.erase(i)
	
	# game_timer
	
	if game_timer > 270:
		if game_stage != 1:
			game_stage = 1
			update_game_size()
	elif game_timer > 180:
		if game_stage != 2:
			game_stage = 2
			update_game_size()
	elif game_timer > 90:
		if game_stage != 3:
			game_stage = 3
			update_game_size()
	elif game_timer > 0:
		if game_stage != 4:
			game_stage = 4
			update_game_size()

	if connection.size() > 1:
		
		if players_wait_timer > 0:
			
			if in_game.size() > 1:
				players_wait_timer -= delta
				
		else:
			
			if game_timer > 0:
				if game_timer_stop: 
					game_timer_stop = false
					broadcast_game_timer("stop")
					move_to_respawn()
				game_timer -= delta
				
			else:
				
				if team_points[TEAM_RED] > team_points[TEAM_BLU]:
					if win_condition != "red":
						win_condition = "red"
						update_feed(["win", TEAM_RED])
				elif team_points[TEAM_BLU] > team_points[TEAM_RED]:
					if win_condition != "blu":
						win_condition = "blu"
						update_feed(["win", TEAM_BLU])
				else:
					win_condition = "spare"
	else:
		
		if players_wait_timer != WAIT_TIME:
			
			team_points = [0, 0, 0, 0]
			game_timer = MATCH_TIME
			players_wait_timer = WAIT_TIME
			
			for i in range(DEFAULT_POINTS.size()):
				var p = points[i]["pos"]
				points[i] = DEFAULT_POINTS[i].duplicate()
				points[i]["pos"] = p
			
			for client in connection:
				send_points(client)
			game_timer_stop = true
			win_condition = ""
			broadcast_game_timer("stop")
	
	if int(game_timer) != prev_game_timer:
		broadcast_game_timer()
		prev_game_timer = int(game_timer)
	
	if int(players_wait_timer) != prev_players_wait_timer:
		broadcast_game_timer("wait")
		prev_players_wait_timer = int(players_wait_timer)


func move_to_respawn():
	for client in connection:
		
		connection[client].player["stats"] = [0, 0, 0]
		broadcast_stats(connection[client].id, connection[client].player["stats"])
		
		for c in connection:
			connection[c].peer.put_var([PLAYER_DATA, "reset", str(connection[client].id), DEFAULT_PLAYER.duplicate(true)])
		var seat
		var p = points[1 - connection[client].player["team"]]
		if p["seats"].has(connection[client].seat):
			p["seats"].erase(connection[client].seat)
		while true:
			seat = randi()%8
			if !seat in p["seats"]:
				break
		p["seats"].append(seat)
		connection[client].seat = seat
		connection[client].peer.put_var([PLAYER_DATA, "spawn_pos", p["pos"] + Vector2(-336 + 96*seat, 0)])


func restart():
	
	move_to_respawn()
	team_points = [0, 0, 0, 0]
	game_timer = MATCH_TIME
	players_wait_timer = WAIT_TIME
	feed = []
	for i in range(DEFAULT_POINTS.size()):
		points[i] = DEFAULT_POINTS[i].duplicate()
	
	state.print_log("Generating map...")
	randomize()
	$map.generate_map()
	
	yield($map, "generation_end")
	
	for i in points.size():
		points[i]["pos"] = $map.map_to_world($map.key_points[i])
	
	for client in connection:
		send_map(client)
		send_points(client)
		assign_team(client)
	update_game_size()
	game_timer_stop = true
	win_condition = ""
	broadcast_game_timer("stop")


func unreliable_packet(id,raw_data):
	
	var data = bytes2var(raw_data)
	
	if unreliable_id.has(data[0]):
		
		if data[1] == "hitbox":
			unreliable_id[data[0]].player["hitbox"] = data[2].duplicate()
			for j in unreliable_id.keys():
				if j in get_tree().multiplayer.get_network_connected_peers():
					get_tree().multiplayer.send_bytes(raw_data, j, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
			return
		
		if unreliable_id[data[0]].player.has(data[1]):
			
			unreliable_id[data[0]].player[data[1]] = data[2]
			
			if error_log.has(data[0]): 
				if error_log[data[0]] > 0: error_log[data[0]] -= 1
			
			for j in unreliable_id.keys():
				
				if j == data[0]:
					continue
				
				if j in get_tree().multiplayer.get_network_connected_peers():
					get_tree().multiplayer.send_bytes(raw_data, j, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)
					if error_log.has(j): 
						if error_log[j] > 0: error_log[j] -= 1
				else:
					state.print_log("Dropped packet for " + str(j))
					if !error_log.has(j): error_log[j] = 0
					error_log[j] += 1
			
			if data[1] == "shoot": unreliable_id[data[0]].player[data[1]] = false
			
		else:
			state.print_log("Wrong packet key!")
			if !error_log.has(data[0]): error_log[data[0]] = 0
			error_log[data[0]] += 1
	else:
		state.print_log("Wrong packet id!")


func update_team_points(last_team,points):
	if team_points[0] + team_points[1] > team_points_max and last_team > -1:
		team_points[1 - last_team] -= team_points[TEAM_RED] + team_points[TEAM_BLU] - team_points_max
	
	team_points[0] = clamp(team_points[TEAM_RED], 0, team_points_max)
	team_points[1] = clamp(team_points[TEAM_BLU], 0, team_points_max)
	team_points[2] = (team_points[TEAM_BLU] - team_points[TEAM_RED]) + (teams[TEAM_BLU].size() - teams[TEAM_RED].size()) * 100
	team_points[3] = (team_points[TEAM_RED] - team_points[TEAM_BLU]) + (teams[TEAM_RED].size() - teams[TEAM_BLU].size()) * 100
	
	var data = [TEAM_POINTS, team_points, team_points_max]
	for client in connection:
		connection[client].peer.put_var(data)


func update_feed(event=[]):
	
	if event != []:
		feed.append(event)
	if feed.size() == 0: return

	var last_event = feed[feed.size()-1]
	var formated_event = ""
	
	if feed.size() > 1:
		if last_event == feed[feed.size()-2]:
			feed.pop_back()
			return
	
	match last_event[0]:
		"hit": # who, target, part, dmg, part_hp
			
			var who = last_event[1]
			var target = last_event[2]
			var part = last_event[3]
			var dmg = last_event[4]
			var part_hp = last_event[5]
			
			var who_nick = "void"
			if who != "void":
				who_nick = unreliable_id[int(who)].info["nickname"]
				
				if part_hp < 1 and players_wait_timer < 0:
					team_points[unreliable_id[int(who)].player["team"]] += 20
					update_team_points(unreliable_id[int(who)].player["team"], 20)
				
			var target_nick = unreliable_id[int(target)].info["nickname"]
			
			formated_event = who_nick + " deals " + str(dmg) + " damage to " + part + " part of " + target_nick

			
		"kill": # target, who
			
			var target = last_event[1]
			var who = last_event[2]
			
			var target_nick = unreliable_id[int(target)].info["nickname"]
			var who_nick = "void"
			if who != "void":
				who_nick = unreliable_id[int(who)].info["nickname"]
				formated_event = who_nick + " killed " + target_nick + "!"
				
				if players_wait_timer < 0:
					team_points[unreliable_id[int(who)].player["team"]] += 100
					update_team_points(unreliable_id[int(who)].player["team"], 100)
				broadcast_feed_event("kill", [int(who), int(target)])
				
				unreliable_id[int(who)].player["stats"][0] += 1
				broadcast_stats(int(who), unreliable_id[int(who)].player["stats"])
				
			else:
				formated_event = "void eats " + target_nick + "..."
				broadcast_feed_event("kill", [-1, int(target)])
				
			if in_game.has(target): in_game.erase(target)
			
			unreliable_id[int(target)].player["stats"][1] += 1
			broadcast_stats(int(target), unreliable_id[int(target)].player["stats"])
			
		"cap": # int idx, int cap_team, objects capturers
			
			var idx = last_event[1]
			var cap_team = last_event[2]
			var capturers = last_event[3]
			
			var point_name = ""
			var team_name = ""
			var capturers_names = ""
			var capturers_ids = []
			var points = 0
			
			match idx:
				0: point_name = "right base"
				1: point_name = "left base"
				2: point_name = "front right point"
				3: point_name = "mid right point"
				4: point_name = "third right point"
				5: point_name = "rear right point"
				6: point_name = "front left point"
				7: point_name = "mid left point"
				8: point_name = "third left point"
				9: point_name = "rear left point"
		
			match cap_team:
				TEAM_RED: team_name = "red"
				TEAM_BLU: team_name = "blue"
			
			for i in capturers:
				capturers_names += i.info["nickname"] + ", "
				i.player["stats"][2] += 1
				broadcast_stats(i.id, i.player["stats"])
				capturers_ids.append(i.id)
			capturers_names = capturers_names.left(capturers_names.length() - 2)
			
			formated_event = capturers_names + " from team " + team_name + " captured " + point_name + "!" 
			
			broadcast_feed_event("cap", [capturers_ids, cap_team, point_name])
			
			match game_size:
				0:
					match idx:
						0: # right base
							match cap_team:
								TEAM_RED: points = 700
								TEAM_BLU: points = 600
						1: # left base
							match cap_team:
								TEAM_RED: points = 600
								TEAM_BLU: points = 700
						2: # front right 
							match cap_team:
								TEAM_RED: points = 500
								TEAM_BLU: points = 450
						3: # mid right
							match cap_team:
								TEAM_RED: points = 550
								TEAM_BLU: points = 400
						4: # third right
							match cap_team:
								TEAM_RED: points = 600
								TEAM_BLU: points = 350
						5: # rear right
							match cap_team:
								TEAM_RED: points = 650
								TEAM_BLU: points = 300
						6: # front left
							match cap_team:
								TEAM_RED: points = 450
								TEAM_BLU: points = 500
						7: # mid left
							match cap_team:
								TEAM_RED: points = 400
								TEAM_BLU: points = 550
						8: # third left
							match cap_team:
								TEAM_RED: points = 350
								TEAM_BLU: points = 600
						9: # rear left
							match cap_team:
								TEAM_RED: points = 300
								TEAM_BLU: points = 650
				1:
					match idx:
						0: # right base
							match cap_team:
								TEAM_RED: points = 700
								TEAM_BLU: points = 600
						1: # left base
							match cap_team:
								TEAM_RED: points = 600
								TEAM_BLU: points = 700
						2: # front right 
							match cap_team:
								TEAM_RED: points = 500
								TEAM_BLU: points = 450
						3: # mid right
							match cap_team:
								TEAM_RED: points = 575
								TEAM_BLU: points = 375
						4: # third right
							match cap_team:
								TEAM_RED: points = 650
								TEAM_BLU: points = 300
						6: # front left
							match cap_team:
								TEAM_RED: points = 450
								TEAM_BLU: points = 500
						7: # mid left
							match cap_team:
								TEAM_RED: points = 375
								TEAM_BLU: points = 575
						8: # third left
							match cap_team:
								TEAM_RED: points = 300
								TEAM_BLU: points = 650
				2:
					match idx:
						0: # right base
							match cap_team:
								TEAM_RED: points = 700
								TEAM_BLU: points = 600
						1: # left base
							match cap_team:
								TEAM_RED: points = 600
								TEAM_BLU: points = 700
						2: # front right 
							match cap_team:
								TEAM_RED: points = 500
								TEAM_BLU: points = 450
						3: # mid right
							match cap_team:
								TEAM_RED: points = 650
								TEAM_BLU: points = 300
						6: # front left
							match cap_team:
								TEAM_RED: points = 450
								TEAM_BLU: points = 500
						7: # mid left
							match cap_team:
								TEAM_RED: points = 300
								TEAM_BLU: points = 650
				3:
					match idx:
						0: # right base
							match cap_team:
								TEAM_RED: points = 700
								TEAM_BLU: points = 600
						1: # left base
							match cap_team:
								TEAM_RED: points = 600
								TEAM_BLU: points = 700
						2: # front right 
							match cap_team:
								TEAM_RED: points = 650
								TEAM_BLU: points = 300
						6: # front left
							match cap_team:
								TEAM_RED: points = 300
								TEAM_BLU: points = 650
			
			team_points[cap_team] += points
			update_team_points(cap_team,points)
			
		"res": # who
		
			var who = last_event[1]
			var who_nick = unreliable_id[int(who)].info["nickname"]
			
			formated_event = who_nick + " respawned"
			
			broadcast_feed_event("res",int(who))
			in_game.append(who)
			
		"win": # team
			
			var team = last_event[1]
			var team_name = ""
			
			match team:
				TEAM_RED: team_name = "red"
				TEAM_BLU: team_name = "blue"
				
			formated_event = "team " + team_name + " wins?"
			
			broadcast_feed_event("win", team)
			
			restart()
	
	state.print_log("New feed event: ", formated_event)


func update_game_size():
	
	var player_count = connection.size()
	
	if player_count-1 in range(4):
		game_size = 3
	elif player_count-1 in range(4, 8):
		game_size = 2
	elif player_count-1 in range(8, 12):
		game_size = 1
	elif player_count-1 in range(12,16):
		game_size = 0
	
	match game_size:
		3:
			for i in [6, 2]:
				if points[i].state == 0: points[i].state = 1
			for i in [7, 8, 9, 3, 4, 5]:
				points[i].state = 0
			
			match game_stage:
				1: team_points_max = 2275
				2: team_points_max = 3155
				3: team_points_max = 3925
				4: team_points_max = 4705
			
		2:
			for i in [6,7,2,3]:
				if points[i].state == 0: points[i].state = 1
			for i in [8,9,4,5]:
				points[i].state = 0
			
			match game_stage:
				1: team_points_max = 3155
				2: team_points_max = 3155
				3: team_points_max = 3925
				4: team_points_max = 4705
			
		1:
			for i in [6, 7, 8, 2, 3, 4]:
				if points[i].state == 0: points[i].state = 1
			for i in [9, 5]:
				points[i].state = 0
			
			match game_stage:
				1: team_points_max = 3925
				2: team_points_max = 3925
				3: team_points_max = 3925
				4: team_points_max = 4705
			
		0:
			for i in [6, 7, 8, 9, 2, 3, 4, 5]:
				if points[i].state == 0: points[i].state = 1
			
			match game_stage:
				1: team_points_max = 4705
				2: team_points_max = 4705
				3: team_points_max = 4705
				4: team_points_max = 4705
	
	update_team_points(-1,0)


func assign_team(who):
	var new = true
	
	for i in teams:
		if i.has(who):
			new = false
			connection[who].player["team"] = teams.find(i)
	
	if new:
		if teams[TEAM_RED].size() > teams[TEAM_BLU].size():
			teams[TEAM_BLU].append(who)
			connection[who].player["team"] = TEAM_BLU
		else:
			teams[TEAM_RED].append(who)
			connection[who].player["team"] = TEAM_RED
	
	var data = [PLAYER_DATA, "team", connection[who].id, connection[who].player["team"]]
	
	for client in connection:
		connection[client].peer.put_var(data)


func send_info(who):
	var data = [PLAYER_CONNECT, 1]
	for client in connection:
		if client == who:
			continue
		data.append([connection[client].id, connection[client].info])
	connection[who].peer.put_var(data)


func send_map(who):
	var packet = []
	for cell in $map.cells:
		if !connection.has(who): return
		if packet.size() < 4096:
			packet.append(cell)
		else:
			connection[who].peer.put_var([MAP,"map", packet])
			packet = [cell]
	if packet.size() > 0:
		connection[who].peer.put_var([MAP, "map", packet])
	connection[who].peer.put_var([MAP, "info", $map.key_points, $map.height])


func send_players(who):
	var data = [PLAYER_DATA,"init"]
	for client in connection:
		
		if client == who:
			continue
		
		data.append([connection[client].id, connection[client].player])
	
	connection[who].peer.put_var(data)


func send_points(who):
	var data = [POINT, 42, points]
	connection[who].peer.put_var(data)
	# hah lol another points in the same func XXX---D
	data = [TEAM_POINTS, team_points, team_points_max]
	connection[who].peer.put_var(data)


func send_stats(who):
	var data = [STATS, -1, []]
	
	for client in connection:
		data[2].append([connection[client].id, connection[client].player["stats"]])
	
	connection[who].peer.put_var(data)


func broadcast_stats(id, stats):
	
	var data = [STATS, id, stats]
	
	for client in connection:
		connection[client].peer.put_var(data)


func broadcast_game_timer(what = "time"):
	var data = [TIMER, what]
	
	match what:
		"time": data.append(game_timer)
		"stop": data.append(game_timer_stop)
		"wait": data.append(prev_players_wait_timer)
	
	for client in connection:
		connection[client].peer.put_var(data)


func broadcast_feed_event(type,d):
	var data = [FEED, type, d]
	for client in connection:
		connection[client].peer.put_var(data)


func broadcast_point(idx, what, val):
	var data = [POINT, idx, what, val]
	for client in connection:
		connection[client].peer.put_var(data)


func broadcast_player(who):
	var data = [PLAYER_DATA, "reset", connection[who].id, connection[who].player]
	for client in connection:
		connection[client].peer.put_var(data)


func broadcast_umer_muzhik(who):
	var data = [PLAYER_DATA, "умер мужик", connection[who].id]
	for i in points:
		if i["capturers"].has(connection[who]):
			i["capturers"].erase(connection[who])
			break
	for client in connection:
		if client == who: continue
		connection[client].peer.put_var(data)


func broadcast_disconnect(who):
	state.print_log(connection[who].info["nickname"] + " disconnected")
	var data = [PLAYER_DISCONNECT, connection[who].id]
	unreliable_id.erase(connection[who].id)
	respawn_timers.erase(who)
	if in_game.has(str(connection[who].id)): in_game.erase(str(connection[who].id))
	for i in points:
		if i["capturers"].has(connection[who]):
			i["capturers"].erase(connection[who])
			break
	connection.erase(who)
	for i in teams:
		if i.has(who):
			i.erase(who)
	for client in connection:
		connection[client].peer.put_var(data)
		
	var nicknames = ""
	for c in connection:
		nicknames+=connection[c].info["nickname"] + ", "
	
	if nicknames == "":
		state.print_log("Server empty!")
	else:
		state.print_log("Currently on server: " + nicknames.left(nicknames.length()-2))


func broadcast_connect(who):
	unreliable_id[connection[who].id] = connection[who]
	
	var data = [PLAYER_CONNECT, 0, connection[who].id, connection[who].info]
	for client in connection:
		
		if client == who:
			continue
		
		connection[client].peer.put_var(data)


func broadcast_player_data(who, data):
	data.append(connection[who].id)
	for client in connection:
		
		if client == who:
			continue
		
		connection[client].peer.put_var(data)
