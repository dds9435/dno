extends TileMap

var map_loaded = false
var key_points = []
var height = 0
var structures_data = null
var temp_cells = null
var temp_bgcells = null
var temp_points = null

func load_map(cells):
	temp_cells = cells
	temp_bgcells = []
	temp_points = []
	for i in cells:
		set_cell(i.x,i.y,randi()%8)
		if Vector2(i.x,i.y).distance_to(Vector2(0,0)) < 100 and get_cell(-i.x,-i.y) == -1:
			if randi()%8 == 0:
				set_cell(-i.x,-i.y,randi()%28+28)
			else:
				set_cell(-i.x,-i.y,randi()%8+16)
			temp_bgcells.append(Vector2(-i.x,-i.y))
	map_loaded = true
	keypoints()
	
	$"/root/game/fg/hud/minimap/drawer".load_map(temp_cells,temp_bgcells,temp_points,get_used_rect().size)

func keypoints():
	var f = File.new()
	var points = ["point1"]
	var bases_right = ["base_right01", "base_right02", "base_right03"]
	var bases_left = ["base_left01", "base_left02", "base_left03"]
	if f.open("res://assets/structures.db",File.READ) == 0:
		structures_data = f.get_var()
		f.close()
	else:
		return
	var counter = 0
	for i in key_points:
		temp_points.append(i)
		if counter == 0:
			place_structure(bases_right[height],i)
		elif counter == 1:
			place_structure(bases_left[height],i)
		else:
			place_structure(points[randi()%points.size()],i)
		counter+=1

func place_structure(n,p):
	if structures_data == null:
		return
	if structures_data.has(n):
		for i in structures_data[n]["tilemap"].keys():
			for j in structures_data[n]["tilemap"][i]:
				var id = int(i)
				if id < 16: temp_cells.append(Vector2(int(j.x)+p.x,int(j.y)+p.y))
				else: temp_bgcells.append(Vector2(int(j.x)+p.x,int(j.y)+p.y))
				match id:
					0: id = randi()%8
					8: id = randi()%8+8
					16: 
						id = randi()%28+16
						if id > 23:
							id+=4
						id = clamp(id,0,55)
					44: id = randi()%4+40
				set_cell(int(j.x)+p.x,int(j.y)+p.y,id)

