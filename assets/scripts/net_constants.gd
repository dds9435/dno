extends Node

const DEFAULT_PORT = 10568
const ID_SRV = 1

const TEAM_RED = 0
const TEAM_BLU = 1

const DEFAULT_PLAYER_RESPAWN_TIME = 3.0
const MIN_CUP_TIME = 3.0

const MATCH_TIME = 360.0
const WAIT_TIME = 15.0

const PLAYER_CONNECT = 0
const PLAYER_DISCONNECT = 1
const PLAYER_DATA = 2
const MESSAGE = 3
const ID = 4
const MAP = 5
const HIT = 6
const POINT = 7
const FEED = 8
const TEAM_POINTS = 9
const TIMER = 10
const STATS = 11
const ALIVE = 12
const EXIT = 13

const ALIVE_TIMER_RATE = 5.0
const ALIVE_TIMER_TIMEOUT = 15.0

const DEFAULT_POINTS = [
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME * 2,
		"seats" : []
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME * 2,
		"seats" : []
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	},
	{
		"progress" : 0.0,
		"state" : 0,
		"cap_team" : 0,
		"capturers" : [],
		"pos" : Vector2(0, 0),
		"cap_time" : MIN_CUP_TIME
	}
]

const DEFAULT_PLAYER = {
	"playing" : false,
	"pos" : Vector2(0, -5000),
	"vel" : Vector2(0, 0),
	"mouse_pos" : Vector2(0, 0),
	"crouch" : false,
	"walk_left" : false,
	"walk_right" : false,
	"jump" : false,
	"jet" : false,
	"jet_fuel" : 0.0,
	"weapon_id" : null,
	"shoot" : false,
	"util_rot" : 0.0,
	"team" : TEAM_RED,
	"respawn" : DEFAULT_PLAYER_RESPAWN_TIME,
	"hitbox" : {
		"lt" : 10,
		"rt" : 10,
		"lb" : 10,
		"rb" : 10,
		"c" : 5
	},
	"stats" : [0, 0, 0] # K, D, C
}