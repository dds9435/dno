extends KinematicBody2D

const GRAVITY = 980
const WALK_SPEED = 150
const JUMP_SPEED = 450
const JET_SPEED = 250

var WALK_MAX_SPEED = 650
var MAX_JUMPS = 1
var WALK_SPEED_FACTOR = 1
var JUMP_SPEED_FACTOR = 1
var JET_SPEED_FACTOR = 1
var JET_RESTORE_FACTOR = 5
var JET_FUEL_MAX = 4.5

var crouch = false
var walk_left = false
var walk_right = false
var jump = false
var jet = false

var velocity = Vector2(0,0)
var jumps = MAX_JUMPS
var onair_time = 0.0
var onair_jet_time = 0.0
var jet_fuel = JET_FUEL_MAX
var jet_activate = 0.0
var jet_fuel_prev = 0.0
var jet_fuel_timer = 0.0

var orientation = false # false - right, true - left
var orientation_old = false
var walk_old = false
var walk_reverse = false 
var anim_pos = 0.0
var oseek = 0.0
var anim
var new_anim
var prev_thrust_anim = ""
var crouching = false
var crouching_offset_target_y = 0
var legs_default_y = 40
var legs_graphics_default_y = 42
var legs_anim_backwards = false

onready var hitbox_graphics_map = {
	"lt" : $"graphics/base/corpus_lt",
	"rt" : $"graphics/base/corpus_rt",
	"lb" : $"graphics/base/corpus_lb",
	"rb" : $"graphics/base/corpus_rb",
	"c" : $"graphics/base/center",
	"eye" : $"graphics/base/eye",
	"leg01" : $"graphics/legs/leg01",
	"leg02" : $"graphics/legs/leg02"
}

onready var hitbox_collision_map = {
	"lt" : $"hitbox/lt",
	"rt" : $"hitbox/rt",
	"lb" : $"hitbox/lb",
	"rb" : $"hitbox/rb",
	"c" : $"hitbox/c"
}

var local_hitbox = {
	"lt" : 10,
	"rt" : 10,
	"lb" : 10,
	"rb" : 10,
	"c" : 5
}

var local_hitbox_max = {
	"lt" : 10,
	"rt" : 10,
	"lb" : 10,
	"rb" : 10,
	"c" : 5
}

var weapon_id = "02"
var speed_cap = 0

var steps = [preload("res://assets/sounds/STEP1.wav"),preload("res://assets/sounds/STEP2.wav"),preload("res://assets/sounds/STEP3.wav")]

var prev_pos = Vector2(0,0)
var prev_vel = Vector2(0,0)
var prev_mouse_pos = Vector2(0,0)
var prev_crouch = false
var prev_walk_left = false
var prev_walk_right = false
var prev_jump = false
var prev_jet = false
var prev_weapon_id = null
var prev_hitbox = {}
var prev_jet_fuel = 0.0

var sync_timer = 1.0

var slaved = false

var to_send_velocity = Vector2(0,0)
var mouse_pos = Vector2(0,0)

var prev_team = -1
var setted_team = -1

func _ready():
	for i in $graphics/base.get_children():
		if i.material: i.material = i.material.duplicate()

func sound_footstep(what):
	match what:
		0:
			$footsteps1.stop()
			$footsteps1.stream = steps[randi()%steps.size()]
			$footsteps1.play()
		1:
			$footsteps2.stop()
			$footsteps2.stream = steps[randi()%steps.size()]
			$footsteps2.play()

func set_team(team):
	
	var color = Color(1,1,1)
	
	match team:
		0:
			color = Color(1,0.31,0.31)
		1:
			color = Color(0.31,0.31,1)
	
	if !slaved:
		for i in $"/root/game/bg".get_children():
			i.get_node("tex").modulate = color.lightened(0.25)
		VisualServer.set_default_clear_color(color.lightened(0.5))
	
	$graphics/base/center/team.modulate = color.lightened(0.1)
	$graphics/base/eye/team.modulate = color.lightened(0.25)
	$graphics/legs/leg01/c/bar.modulate = color.lightened(0.25)
	$graphics/legs/leg02/c/bar.modulate = color.lightened(0.25)
	$light.color = color.lightened(0.25)
	
	setted_team = team

func visual_damage(d,target_part):
	
	local_hitbox[target_part] = d
	hitbox_graphics_map[target_part].get_node("hit_anim").play("hit")
	
	var max_hp = local_hitbox_max[target_part]
	var hp = local_hitbox[target_part]
	var force = float(max_hp-hp) / max_hp
	
	if hp < 1:
		hitbox_graphics_map[target_part].hide()
		hitbox_collision_map[target_part].set_collision_layer_bit(4,false)
		hitbox_collision_map[target_part].set_collision_mask_bit(4,false)
	else:
		if !hitbox_graphics_map[target_part].visible:
			hitbox_graphics_map[target_part].show()
			hitbox_collision_map[target_part].set_collision_layer_bit(4,true)
			hitbox_collision_map[target_part].set_collision_mask_bit(4,true)
		hitbox_graphics_map[target_part].material.set_shader_param("force",force)
	

func _physics_process(delta):
	
	# map
	
	if !has_node("/root/game/fg/hud/minimap/drawer/"+name):
		$"/root/game/fg/hud/minimap/drawer".add_player(name)
	else:
		get_node("/root/game/fg/hud/minimap/drawer/"+name).position = position / 32 + $"/root/game/fg/hud/minimap/drawer".size/2
		if get_node("/root/game/fg/hud/minimap/drawer/"+name).team != setted_team:
			get_node("/root/game/fg/hud/minimap/drawer/"+name).team = setted_team
			get_node("/root/game/fg/hud/minimap/drawer/"+name).u()
		if get_node("/root/game/fg/hud/minimap/drawer/"+name).crouch != crouching:
			get_node("/root/game/fg/hud/minimap/drawer/"+name).crouch = crouching
			get_node("/root/game/fg/hud/minimap/drawer/"+name).u()
		
	
	if name != str(networkv2.info["id"]):
		
		slaved = true
		
		if prev_pos != networkv2.players[int(name)]["pos"]:
			position = networkv2.players[int(name)]["pos"]
			prev_pos = networkv2.players[int(name)]["pos"]
		
		if prev_vel != networkv2.players[int(name)]["vel"]:
			velocity = networkv2.players[int(name)]["vel"]
			prev_vel = networkv2.players[int(name)]["vel"]
			
		if prev_mouse_pos != networkv2.players[int(name)]["mouse_pos"]:
			mouse_pos = networkv2.players[int(name)]["mouse_pos"]
			prev_mouse_pos = networkv2.players[int(name)]["mouse_pos"]
		
		if prev_crouch != networkv2.players[int(name)]["crouch"]:
			crouch = networkv2.players[int(name)]["crouch"]
			prev_crouch = networkv2.players[int(name)]["crouch"]
		
		if prev_walk_left != networkv2.players[int(name)]["walk_left"]:
			walk_left = networkv2.players[int(name)]["walk_left"]
			prev_walk_left = networkv2.players[int(name)]["walk_left"]
		
		if prev_walk_right != networkv2.players[int(name)]["walk_right"]:
			walk_right = networkv2.players[int(name)]["walk_right"]
			prev_walk_right = networkv2.players[int(name)]["walk_right"]
		
		if prev_jump != networkv2.players[int(name)]["jump"]:
			jump = networkv2.players[int(name)]["jump"]
			prev_jump = networkv2.players[int(name)]["jump"]
		
		if prev_jet != networkv2.players[int(name)]["jet"]:
			jet = networkv2.players[int(name)]["jet"]
			prev_jet = networkv2.players[int(name)]["jet"]
		
		if prev_weapon_id != networkv2.players[int(name)]["weapon_id"]:
			weapon_id = networkv2.players[int(name)]["weapon_id"]
			prev_weapon_id = networkv2.players[int(name)]["weapon_id"]
			$weapon.init_weapon(weapon_id)
		
		if prev_team != networkv2.players[int(name)]["team"]:
			set_team(networkv2.players[int(name)]["team"])
			prev_team = networkv2.players[int(name)]["team"]
		
		if prev_jet_fuel != networkv2.players[int(name)]["jet_fuel"]:
			jet_fuel = networkv2.players[int(name)]["jet_fuel"]
			prev_jet_fuel = networkv2.players[int(name)]["jet_fuel"]
		
		$weapon.look_at(mouse_pos)
		$weapon.orientation = orientation
		
		if (mouse_pos - position).x > 0: orientation = false
		else: orientation = true
	
	else:
		
		slaved = false
		
		if sync_timer > 0:
			sync_timer-=delta
			if sync_timer < 0:
				networkv2.send_data("jet",jet)
				networkv2.send_data("weapon_id",weapon_id)
				sync_timer = 1.0
		
		if prev_pos != position.snapped(Vector2(0.1,0.1)):
			networkv2.send_data("pos",position.snapped(Vector2(0.1,0.1)))
			prev_pos = position.snapped(Vector2(0.1,0.1))
		
		if prev_vel != to_send_velocity.snapped(Vector2(0.1,0.1)):
			networkv2.send_data("vel",to_send_velocity.snapped(Vector2(0.1,0.1)))
			prev_vel = to_send_velocity.snapped(Vector2(0.1,0.1))
		
		if prev_mouse_pos != get_global_mouse_position().snapped(Vector2(1,1)):
			networkv2.send_data("mouse_pos",get_global_mouse_position().snapped(Vector2(1,1)))
			prev_mouse_pos = get_global_mouse_position().snapped(Vector2(1,1))
		
		if prev_crouch != crouch:
			networkv2.send_data("crouch",crouch)
			prev_crouch = crouch
		
		if prev_walk_left != walk_left:
			networkv2.send_data("walk_left",walk_left)
			prev_walk_left = walk_left
		
		if prev_walk_right != walk_right:
			networkv2.send_data("walk_right",walk_right)
			prev_walk_right = walk_right
		
		if prev_jump != jump:
			networkv2.send_data("jump",jump)
			prev_jump = jump
		
		if prev_jet != jet:
			networkv2.send_data("jet",jet)
			prev_jet = jet
		
		if prev_weapon_id != weapon_id:
			networkv2.send_data("weapon_id",weapon_id)
			prev_weapon_id = weapon_id
			$weapon.init_weapon(weapon_id)
		
		if prev_hitbox.keys() != local_hitbox.keys() and prev_hitbox.values() != local_hitbox.values():
			networkv2.send_data("hitbox",local_hitbox)
			prev_hitbox = local_hitbox.duplicate()
		
		if prev_team != $"/root/game".team:
			set_team($"/root/game".team)
			prev_team = $"/root/game".team
		
		if prev_jet_fuel != jet_fuel:
			networkv2.send_data("jet_fuel",jet_fuel)
			prev_jet_fuel = jet_fuel
		
		if !$"/root/game".ui_busy:
			crouch = Input.is_action_pressed("crouch")
			walk_left = Input.is_action_pressed("walk_left")
			walk_right = Input.is_action_pressed("walk_right")
			jump = Input.is_action_just_pressed("jump")
			jet = Input.is_action_pressed("jump")
			$weapon.look_at(get_global_mouse_position())
			if (get_global_mouse_position() - position).x > 0: orientation = false
			else: orientation = true
		else:
			crouch = false
			walk_left = false
			walk_right = false
			jump = false
			jet = false
	
		$weapon.orientation = orientation
	
	# leg indicator
	
	$graphics/legs/leg01/c/bar.value = jet_fuel / JET_FUEL_MAX * 100 + 15
	$graphics/legs/leg02/c/bar.value = jet_fuel / JET_FUEL_MAX * 100 + 15
	
	# mirroring
	
	if orientation_old != orientation:
		if anim == "walk": oseek = $legs_anim.current_animation_position
		anim = ""
		orientation_old = orientation
	if walk_left and !walk_old:
		anim = ""
		velocity.x = 0
		walk_old = true
	if walk_right and walk_old:
		anim = ""
		velocity.x = 0
		walk_old = false
	
	if orientation:
		if walk_left: walk_reverse = false
		else: walk_reverse = true
		$graphics.scale.x = -1
		$hitbox.scale.x = -1
	else:
		if walk_right: walk_reverse = false
		else: walk_reverse = true
		$graphics.scale.x = 1
		$hitbox.scale.x = 1
	
	###
	
	if !slaved:
		if walk_left:
			if abs(velocity.x) < WALK_MAX_SPEED:
				if abs(velocity.x) < WALK_SPEED*WALK_SPEED_FACTOR: velocity.x=-WALK_SPEED*WALK_SPEED_FACTOR
				velocity.x-=WALK_SPEED*WALK_SPEED_FACTOR*delta
		if walk_right:
			if abs(velocity.x) < WALK_MAX_SPEED:
				if abs(velocity.x) < WALK_SPEED*WALK_SPEED_FACTOR: velocity.x=WALK_SPEED*WALK_SPEED_FACTOR
				velocity.x+=WALK_SPEED*WALK_SPEED_FACTOR*delta
		if jump and jumps > 0:
			velocity.y = -JUMP_SPEED*JUMP_SPEED_FACTOR
			jumps-=1 
	if jet: jet_activate+=delta
	if !slaved:
		if jet and jet_activate > 0.3 and jet_fuel > 0:
			velocity.y = -JET_SPEED*JET_SPEED_FACTOR
			jet_fuel-=delta
			
		if !walk_left and !walk_right:
			velocity.x = lerp(velocity.x,0,0.3)

	# crouching 
	
	if crouch:
		crouching = true
	else:
		if !$rays/crouch_0.is_colliding() and !$rays/crouch_1.is_colliding(): crouching = false 
	
	if crouching:
		if crouching_offset_target_y == 0:
			crouching_offset_target_y = -16
	else:
		if crouching_offset_target_y == -16:
			crouching_offset_target_y = 0
	$legs.position.y = lerp($legs.position.y,legs_default_y + crouching_offset_target_y,0.4)
	$graphics/legs.position.y = lerp($graphics/legs.position.y,legs_graphics_default_y + crouching_offset_target_y,0.4)
	
	# onair
	
	if velocity.y != 0 and !is_on_floor():
		onair_time += delta
		if jet and jet_activate > 0.3 and jet_fuel > 0:
			if $graphics/legs/leg01/thruster.animation == "open" and $graphics/legs/leg01/thruster.frame == 7:
				$graphics/legs/leg01/thruster.animation = "fire"
				$graphics/legs/leg02/thruster.animation = "fire"
			elif $graphics/legs/leg01/thruster.animation != "fire":
				$graphics/legs/leg01/thruster.animation = "open"
				$graphics/legs/leg02/thruster.animation = "open"
			onair_jet_time += delta
		else:
			$graphics/legs/leg01/thruster.animation = "close"
			$graphics/legs/leg02/thruster.animation = "close"
		
		if $graphics/legs/leg01/thruster.animation != prev_thrust_anim:
			$graphics/legs/thrust_anim.play($graphics/legs/leg01/thruster.animation)
			prev_thrust_anim = $graphics/legs/leg01/thruster.animation
	
	if is_on_floor():
		if onair_time > 0.1:
			onair_time = 0.0
			onair_jet_time = 0.0
			jumps = MAX_JUMPS
			jet_activate = 0.0
			sound_footstep(0)
		if jet_fuel < JET_FUEL_MAX: jet_fuel+=delta*JET_RESTORE_FACTOR
		if jet_fuel > JET_FUEL_MAX: jet_fuel = JET_FUEL_MAX
	
	# animation
	
	if $legs_anim.playback_speed != abs(velocity.x)/50 and abs(velocity.x)/50 > 1:
		$legs_anim.playback_speed = abs(velocity.x)/50
		$base_anim.playback_speed = abs(velocity.x)/50

	if anim == "walk":
		anim_pos = $legs_anim.current_animation_position
	
	if onair_time > 0.1:
		var a
		if velocity.y < 0: 
			new_anim = "jump"
			a = (velocity.angle() + PI/2) * $graphics.scale.x + deg2rad(10)
		else: 
			a = (velocity.angle() + PI/2) * $graphics.scale.x * -$graphics.scale.x * 0.15
			new_anim = "fall"
			if jet_activate <= 0.3 and jumps > 0:
				jet_activate = 0.0
		$graphics/legs/leg01.rotation = lerp($graphics/legs/leg01.rotation,clamp(a,deg2rad(-35),deg2rad(35)),0.2)
		$graphics/legs/leg02.rotation = lerp($graphics/legs/leg02.rotation,clamp(a,deg2rad(-35),deg2rad(35)),0.2)

	else:
		if abs(velocity.x) < 50: new_anim = "idle"
		else: new_anim = "walk"
	
	if anim != new_anim:
		anim = new_anim
		
		var leg_0
		if anim_pos > 0.5 and anim_pos < 1.5:
			leg_0 = false
		else:
			leg_0 = true 
		
		match new_anim:
			"jump":
				$base_anim.play("jump")
				if walk_reverse:
					if leg_0 == walk_reverse: $legs_anim.play("free_0")
					else: $legs_anim.play("free_1")
				else:
					if leg_0 == walk_reverse: $legs_anim.play("free_1")
					else: $legs_anim.play("free_0")
			"fall":
				$base_anim.play("fall")
				if walk_reverse:
					if leg_0 == walk_reverse: $legs_anim.play("free_0")
					else: $legs_anim.play("free_1")
				else:
					if leg_0 == walk_reverse: $legs_anim.play("free_1")
					else: $legs_anim.play("free_0")
			"idle":
				
				if walk_reverse:
					if leg_0 == walk_reverse: $legs_anim.play("idle_0")
					else: $legs_anim.play("idle_1")
				else:
					if leg_0 == walk_reverse: $legs_anim.play("idle_1")
					else: $legs_anim.play("idle_0")
				
				$base_anim.play("idle")
				
			"walk":
				if $legs_anim.current_animation in ["idle_0","free_0"]: leg_0 = true
				else: leg_0 = false
				
				if walk_reverse:
					$base_anim.play_backwards("walk")
					$legs_anim.play_backwards("walk")
					legs_anim_backwards = true
					if !leg_0:
						$legs_anim.seek(1.0)
				else:
					$base_anim.play("walk")
					$legs_anim.play("walk")
					legs_anim_backwards = false
					if !leg_0:
						$legs_anim.seek(1.0)
				if oseek != 0.0:
					$legs_anim.seek(oseek)
					oseek = 0.0
					
	# physics
	
	if !slaved:
		velocity.y += GRAVITY * delta
		to_send_velocity = velocity
	velocity = move_and_slide(velocity,Vector2(0,-1))