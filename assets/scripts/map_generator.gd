extends TileMap

var cells = []
var key_points = [null,null]

var cep_current = 0
var worm_stage = -1
var worm_stage_set = false
var worm_target_y = 0
var worm_x = 0
var worm_y = 0
var height = 0
var second_pass = false

var sample = 2048
var worm_distance_bias = 2
var worm_target_ys = [40,35,15,0,-15,-30,-30,-45,-60,-60,-60,-75,-75,-75,-75,-90,-90,-90,-100,-100]
var circle_enter_points = [-100,-100,-100,-100,-75,-75,-60,-60,-60,-35,-35,0,0,0,0]

signal generation_end
signal worm_end

func generate_map():
	cep_current = 0
	worm_stage = -1
	worm_stage_set = false
	worm_target_y = 0
	worm_x = 0
	worm_y = 0
	height = 0
	second_pass = false
	key_points = [null,null]
	circle(0,0,100,true,true)

	worm()
	
	yield(self,"worm_end")

	if !second_pass:
		cep_current = 0
		second_pass = true
		worm()
	
	yield(self,"worm_end")

	# keypoints
	height = randi()%3
	var pos = []
	
	for i in range(4):
		pos.append(Vector2(16*(i+1),0))
		
	for i in range(2):
		match height:
			0: key_points[i] = (Vector2(125*(1-2*i),0))
			1: key_points[i] = (Vector2(125*(1-2*i),-40))
			2: key_points[i] = (Vector2(135*(1-2*i),-75))
		
		for j in pos:
			var y = -randi()%64+32
			key_points.append(j*Vector2(1-2*i,1)+Vector2(0,y))
			circle(j.x*(1-2*i),y,8)
		
	for i in key_points:
		var angle = randi()%60
		var j_start = i
		for k in range(40):
			circle(j_start.x+4-randi()%8,j_start.y+4-randi()%8,6)
			j_start-=Vector2(4,0).rotated(deg2rad(60+angle))
		
	clean()

func worm():
	while cep_current < circle_enter_points.size():
		
		if worm_stage != cep_current and !worm_stage_set:
			worm_stage = cep_current
			if !second_pass: worm_x = -100
			else: worm_x = 100
			worm_y = circle_enter_points[cep_current]
			worm_target_y = worm_target_ys[randi()%worm_target_ys.size()]
			circle(worm_x,worm_y,4)
			worm_stage_set = true
		
		var worm_distance_to_target_old = Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y))
		if worm_distance_to_target_old > worm_distance_bias:
			for q in range(sample):
				worm_distance_to_target_old = Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y))
				var dir = randi()%4
				match dir:
					0: 
						worm_x+=1
						if worm_distance_to_target_old < Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y)) and randi()%6 == 0:
							worm_x-=1
					1: 
						worm_x-=1
						if worm_distance_to_target_old < Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y)) and randi()%6 == 0:
							worm_x+=1
					2: 
						worm_y+=1
						if worm_distance_to_target_old < Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y)) and randi()%6 == 0:
							worm_y-=1
					3: 
						worm_y-=1
						if worm_distance_to_target_old < Vector2(worm_x,worm_y).distance_to(Vector2(0,worm_target_y)) and randi()%6 == 0:
							worm_y+=1
				circle(worm_x,worm_y,2)
				if worm_distance_to_target_old < worm_distance_bias:
					circle(worm_x,worm_y,4)
					break
		else:
			cep_current+=1
			worm_stage_set = false
		
		yield(get_tree(),"idle_frame")
	emit_signal("worm_end")


func clean(passes=6):
	var counter = 0
	for i in key_points:
		if counter == 0:
			for j in range(16):
				circle(i.x-j*4,i.y+randi()%4-2,randi()%2+6)
		elif counter == 1:
			for j in range(16):
				circle(i.x+j*4,i.y+randi()%4-2,randi()%2+6)
		counter+=1
		if counter > 1: break
	cells = get_used_cells()
	for i in range(passes):
		for cell in cells:
			var score = 0
			for x in range(0,3):
				for y in range(0,3):
					if get_cellv((cell + Vector2(x-1,y-1))) == 0:
						score+=1
			if score < 5:
				set_cellv(cell,-1)
	cells = get_used_cells()
	emit_signal("generation_end")


func circle(x0, y0, radius, fill=false, full_circle=true, d=1):
	var x = radius-1
	var y = 0
	var dx = d
	var dy = d
	var err = dx - (radius << 1)

	while x >= y:
		
		for i in range(x):
			if fill:
				set_cell(x0 + i, y0 + y, 0)
				set_cell(x0 + y, y0 + i, 0)
				set_cell(x0 - y, y0 + i, 0)
				set_cell(x0 - i, y0 + y, 0)
				if full_circle:
					set_cell(x0 - i, y0 - y, 0)
					set_cell(x0 - y, y0 - i, 0)
					set_cell(x0 + y, y0 - i, 0)
					set_cell(x0 + i, y0 - y, 0)
			else:
				set_cell(x0 + i, y0 + y, -1)
				set_cell(x0 + y, y0 + i, -1)
				set_cell(x0 - y, y0 + i, -1)
				set_cell(x0 - i, y0 + y, -1)
				if full_circle:
					set_cell(x0 - i, y0 - y, -1)
					set_cell(x0 - y, y0 - i, -1)
					set_cell(x0 + y, y0 - i, -1)
					set_cell(x0 + i, y0 - y, -1)

			
			
		if err <= 0:
			y+=1
			err += dy
			dy += 2
        
		if (err > 0):
			x-=1
			dx += 2
			err += dx - (radius << 1)