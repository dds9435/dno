extends Control

const EVENT_TIMEOUT = 5.0

onready var feed = $"/root/game/fg/feed_control/feed"
var feed_icon_cap = "res://assets/graphics/ui/hud/feed_cap.png"
var feed_icon_kill = "res://assets/graphics/ui/hud/feed_kill.png"
var feed_icon_fall = "res://assets/graphics/ui/hud/feed_fall.png"

var feed_a = []

var debug_overlay = false
var hide_hud = true
var esc_menu = false

func _ready():
	if !has_node("/root/hud"):
		$"../debug_overlay".visible = debug_overlay
		visible = hide_hud
		
		$"../menu/panel/sound".pressed = state.cfg["sound"]
		$"../menu/panel/fullscreen".pressed = state.cfg["fullscreen"]
	
	set_process(true)

func _process(delta):
	
	for i in feed_a:
		i[1] -= delta
		if i[1] < 0:
			feed_a.erase(i)
			update_feed()
	
	if Input.is_action_just_pressed("debug"):
		debug_overlay = !debug_overlay
		$"../debug_overlay".visible = debug_overlay
		
	if Input.is_action_just_pressed("hide_hud") and !$"/root/game".respawn:
		hide_hud = !hide_hud
		visible = hide_hud
	
	if Input.is_action_just_pressed("escape"):
		esc_menu = !esc_menu
		$"../dot".visible = !esc_menu
		if esc_menu:
			$"../menu/anim".play("show")
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		else:
			$"../menu/anim".play("hide")
			Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
		$"/root/game".ui_busy = esc_menu
	
	if debug_overlay:
		var fps = Performance.get_monitor(Performance.TIME_FPS)
		var ft = Performance.get_monitor(Performance.TIME_PROCESS)
		var player_pos = Vector2(0,0)
		if has_node("/root/game/players/"+str(networkv2.info["id"])):
			player_pos = get_node("/root/game/players/"+str(networkv2.info["id"])).position.snapped(Vector2(1,1))
		$"../debug_overlay".text = "FPS: %s, Frame time: %s, Position: %s" % [str(fps),str(ft),str(player_pos)]


func append_feed(type,data):
	match type:
		"cap":
			
			var capturers = data[0]
			var cap_team = data[1]
			var point_name = data[2]
			
			match cap_team:
				0: cap_team = "#ff9999"
				1: cap_team = "#9999ff"
			
			var capturers_names = ""
			for i in capturers:
				var i_formated = networkv2.get_player_text_data(i)
				capturers_names += i_formated[0] + ", "
			capturers_names = capturers_names.left(capturers_names.length()-2)
			
			feed_a.insert(0,["[color="+cap_team+"]"+capturers_names+" [img]"+feed_icon_cap+"[/img] [color=#ffffff]"+point_name+"[/color]",EVENT_TIMEOUT])
			
		"kill":
			
			var who = data[0]
			var target = data[1]
			
			var who_formated = networkv2.get_player_text_data(who)
			var target_formated = networkv2.get_player_text_data(target)
			
			if who_formated != []:
				feed_a.insert(0,["[color="+who_formated[1]+"]"+who_formated[0]+"[/color] [img]"+feed_icon_kill+"[/img] [color="+target_formated[1]+"]"+target_formated[0],EVENT_TIMEOUT])
			else:
				feed_a.insert(0,["[img]"+feed_icon_fall+"[/img] [color="+target_formated[1]+"]"+target_formated[0],EVENT_TIMEOUT])
			
		"join":
			
			var who = data[0]
			var connect = data[1]
			
			var who_formated = networkv2.get_player_text_data(who)
			
			if connect:
				connect = "joined"
			else:
				connect = "disconnected"
			
			feed_a.insert(0,["[color="+who_formated[1]+"]"+who_formated[0]+"[/color] "+connect,EVENT_TIMEOUT])
	
	update_feed()

func update_feed():
	if feed_a.size() > 4:
		feed_a.pop_back()
	feed.clear()
	feed.push_align(RichTextLabel.ALIGN_RIGHT)
	for i in feed_a:
		feed.append_bbcode(i[0])
		feed.newline()
	

func update_team_points(team_points,team_points_max):
	var red = float(team_points[0])/team_points_max * 100
	var blue = float(team_points[1])/team_points_max * 100
	var prev_red = $upper_panel/team_points/red.value
	var prev_blue = $upper_panel/team_points/blue.value
	
	$upper_panel/team_points/tw.interpolate_property($upper_panel/team_points/red,"value",prev_red,red,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
	$upper_panel/team_points/tw.start()
	$upper_panel/team_points/tw.interpolate_property($upper_panel/team_points/blue,"value",prev_blue,blue,0.5,Tween.TRANS_QUAD,Tween.EASE_OUT)
	$upper_panel/team_points/tw.start()
	
	$upper_panel/team_points/red_text.text = str(team_points[0])
	$upper_panel/team_points/blue_text.text = str(team_points[1])

func update_points_indicator(idx,state):
	var point = $upper_panel/points_indicator.get_node(str(idx)+"/c")
	
	point.get_node("anim").play("change")
	
	if state == 0:
		point.modulate = Color(1,1,1,0)
		return
	if state == 1:
		point.modulate = Color(1,1,1).darkened(0.1)
	
	match state:
		1: 
			point.modulate = Color(1,1,1).darkened(0.1)
		2: 
			point.modulate = Color(1,0.31,0.31).darkened(0.1)
		3: 
			point.modulate = Color(0.31,0.31,1).darkened(0.1)

func update_timer(game_timer,wait=false):
	if !wait:
		$upper_panel/timer.text = str((game_timer%3600)/60) + ":" + str(game_timer%60).pad_zeros(2)
	else:
		$upper_panel/timer.text = "Waiting for players " + str(game_timer)

func _on_exit_pressed():
	$"/root/game".peer.put_var([13])

func _on_sound_pressed():
	state.cfg["sound"] = $"../menu/panel/sound".pressed
	$"/root/game".sound_update()
	$"../menu/panel/sound".release_focus()

func _on_fullscreen_pressed():
	state.cfg["fullscreen"] = $"../menu/panel/fullscreen".pressed
	$"/root/game".fullscreen_update()
	$"../menu/panel/fullscreen".release_focus()
