extends Node2D

var imgtex = null

var points = []
var size = Vector2(0,0)
	
func remove_player(id):
	if has_node(str(id)): get_node(str(id)).queue_free()

func add_player(str_id):
	if has_node(str_id): return
	
	var p = Node2D.new()
	p.name = str_id
	p.set_script(preload("res://assets/scripts/hud_minimap_unit.gd"))
	add_child(p)
	
func load_map(cells,bgcells,kpoints,s):
	
	points = []
	size = s
	
	var tex = Image.new()
	tex.create(size.x,size.y,false,Image.FORMAT_RGBA8)
	tex.lock()
	
	for i in bgcells:
		tex.set_pixel(i.x + size.x/2,i.y + size.y/2,Color(0.1,0.1,0.1))
	for i in cells:
		tex.set_pixel(i.x + size.x/2,i.y + size.y/2,Color(0.3,0.3,0.3))
	for i in kpoints:
		points.append(Vector2(i.x + size.x/2,i.y + size.y/2))
		
	for i in range(points.size()):
		if !has_node(str(i)):
			var p = Node2D.new()
			p.name = str(i)
			p.position = points[i]
			p.set_script(preload("res://assets/scripts/hud_minimap_unit.gd"))
			add_child(p)
		else:
			get_node(str(i)).position = points[i]
		
	tex.unlock()
	
	imgtex = ImageTexture.new()
	imgtex.create_from_image(tex,0)
	
	update()
	$"..".rect_size = Vector2(256,256)

func _draw():
	draw_texture(imgtex,Vector2(0,0))