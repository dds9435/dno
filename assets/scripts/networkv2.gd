extends "res://assets/scripts/net_constants.gd"

var ip
var port
var info = {
	"nickname" : "",
	"id" : 0
}

var players = Dictionary()
var players_info = Dictionary()


func send_data(what, d):
	var data = [info["id"], what, d]
	if get_tree().multiplayer.has_network_peer():
		get_tree().multiplayer.send_bytes(var2bytes(data), ID_SRV, NetworkedMultiplayerPeer.TRANSFER_MODE_UNRELIABLE_ORDERED)


func send_hit(type, who, target, target_part, point, damage):
	# HIT - 6
	var data = [HIT, info["id"], type, who, target, target_part, point, damage]
	$"/root/game".peer.put_var(data)


func get_player_text_data(id):
	if id == -1:
		return []

	var team_color

	if !players.has(id):
		if id == info["id"]:
			var team = $"/root/game".team
			var nickname = info["nickname"]
			
			match team:
				TEAM_RED: team_color = "#ff9999"
				TEAM_BLU: team_color = "#9999ff"

			return [nickname, team_color]

		return

	var team = players[id]["team"]
	var nickname = players_info[id]["nickname"]

	match team:
		TEAM_RED: team_color = "#ff9999"
		TEAM_BLU: team_color = "#9999ff"

	return [nickname, team_color]