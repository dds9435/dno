extends Node

var fullscreen = false
var sound = true
var ver = "debug"

var cfg = {
	"nickname" : "newbe",
	"ip" : "homelessgunners.tk",
	"port" : 10568,
	"fullscreen" : false,
	"sound" : true
}


func _ready():
	get_tree().get_root().connect("size_changed", self, "resize")
	if fullscreen:
		OS.window_fullscreen = true
    

func print_log(text):
	prints("{year}/{month}/{day} {hour}:{minute}:{second}".format(OS.get_datetime(), text))


func update_sound():
	AudioServer.set_bus_mute(0, !sound)


func resize():
	if has_node("/root/game/fg"):
		for i in $"/root/game/fg".get_children():
			if i is Control:
				i.rect_size = OS.get_window_size()