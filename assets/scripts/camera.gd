extends Camera2D

var respawn = true
var respawning = false

var a = Vector2(0,0)
var b = Vector2(0,0)
var z = 0

func _physics_process(delta):
	
	if !$"/root/game".ui_busy:
		if has_node("../players/"+str(networkv2.info["id"])):
			a = get_node("../players/"+str(networkv2.info["id"])).global_position
		b = get_local_mouse_position()-a
	
	var distance = a.distance_to(b*-1)
	
	
	if respawn:
		position = Vector2(0,0)
		if !respawning:
			z = clamp(lerp(zoom.x,10,0.025),0.75,10)
		else:
			z = clamp(lerp(zoom.x,1,0.001),0.75,10)
	else:
		z = clamp(lerp(zoom.x,(distance/300) * (720/OS.get_window_size().y),0.01),0.75 * (720/OS.get_window_size().y),1.5 * (720/OS.get_window_size().y))
		position = position.linear_interpolate(a + (a+b)/2 - Vector2(0,16),0.3)
		$"/root/game/fg/hud/minimap/drawer".position = - position / 24 - $"/root/game/fg/hud/minimap/drawer".size / 1.6 + Vector2(128,128)
		$"/root/game/fg/hud/minimap".rect_size = Vector2(256,256)
	
	zoom = Vector2(z,z)
	