extends Control

const CONFIG_PATH = "user://dno.cfg"

onready var logo = $"CenterContainer/VBox/logo"
onready var sound = $"CenterContainer/VBox/sound"
onready var fullscreen = $"CenterContainer/VBox/fullscreen"
onready var nickname = $"CenterContainer/VBox/nickname"
onready var ip = $"CenterContainer/VBox/HBox/ip"
onready var port = $"CenterContainer/VBox/HBox/port"

var is_ready = false

func _ready():
	
	$CenterContainer/VBox/version.text = "build: "+state.ver
	
	var config_file = File.new()
	if config_file.open(CONFIG_PATH,File.READ) == OK:
		read_cfg(config_file)
	else:
		new_cfg(config_file)
		
	if !OS.is_debug_build():
		update_dno_pck()
	
	VisualServer.set_default_clear_color(Color(0.5,0.5,0.5))
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	is_ready = true
	for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):
		calc_min_width(menu_ui_node)

func _on_c_pressed():
	networkv2.ip = ip.text
	networkv2.port = int(port.text)
	networkv2.info["nickname"] = nickname.text
	get_tree().change_scene("res://assets/scenes/game.tscn")


func _on_fullscreen_pressed():
	state.fullscreen = fullscreen.pressed
	OS.window_fullscreen = fullscreen.pressed
	state.resize()
	state.cfg["fullscreen"] = fullscreen.pressed
	write_cfg()


func _on_sound_pressed():
	state.sound = sound.pressed
	state.update_sound()
	state.cfg["sound"] = sound.pressed
	write_cfg()


func calc_font_size(min_font_size=150, max_font_size=250, min_width=1280, max_width=2560):
	return (min_font_size + (max_font_size - min_font_size) * ((get_viewport_rect().size.x - min_width) / (max_width - min_width)))


func calc_min_width(ui_node):
	var font_size = ui_node.get('custom_fonts/font').get_size()
	if !ui_node.has_meta('min_chars'):
		ui_node.set_meta('min_chars', ui_node.rect_size.x / (font_size/2))
		return ui_node.rect_size.x
	else:
		return (font_size / 2) * ui_node.get_meta('min_chars')


func _on_main_resized():
	if is_ready:
		logo.get('custom_fonts/font').set_size(calc_font_size())
		for menu_ui_node in get_tree().get_nodes_in_group('menu_ui'):
			menu_ui_node.get('custom_fonts/font').set_size(calc_font_size(16, 26))
			menu_ui_node.rect_min_size.x = calc_min_width(menu_ui_node)


func read_cfg(c):
	var config_file_data = c.get_as_text()
	if validate_json(config_file_data) != "":
		new_cfg(c)
	else:
		config_file_data = parse_json(config_file_data)
		for i in config_file_data.keys():
			if !state.cfg.has(i):
				new_cfg(c)
				return
		
		for i in config_file_data.keys():
			if state.cfg.has(i):
				state.cfg[i] = config_file_data[i]
		
		c.close()
	
	for i in state.cfg.keys():
		match i:
			"nickname":
				if state.cfg[i].length() > 24: 
					state.cfg[i] = "newbe"
					write_cfg()
				$CenterContainer/VBox/nickname.text = state.cfg[i]
			"ip":
				$CenterContainer/VBox/HBox/ip.text = state.cfg[i]
			"port":
				$CenterContainer/VBox/HBox/port.text = str(state.cfg[i])
			"fullscreen":
				$CenterContainer/VBox/fullscreen.pressed = state.cfg[i]
				_on_fullscreen_pressed()
			"sound":
				$CenterContainer/VBox/sound.pressed = state.cfg[i]
				_on_sound_pressed()

func new_cfg(c):
	if c.open(CONFIG_PATH,File.WRITE) != OK:
		print("Can't make config file!")
		c.close()
	else:
		c.store_string(to_json(state.cfg))
		c.close()

func write_cfg():
	var c = File.new()
	
	if c.open(CONFIG_PATH,File.WRITE) != OK:
		print("Can't write to config file!")
		c.close()
	else:
		c.store_string(to_json(state.cfg))
		c.close()

func update_dno_pck():
	
	$http.use_threads = true
	
	$http.download_file = "res://dno.pck"
	if $http.request("http://homelessgunners.tk:10666/dno.pck") == OK:
		print("Updating updater...")
	else:
		print("Can't update updater!")
		return
	
	yield($http,"request_completed")
	print("Updated updater!")

func _on_ip_text_changed(new_text):
	state.cfg["ip"] = new_text
	write_cfg()

func _on_port_text_changed(new_text):
	state.cfg["port"] = int(new_text)
	write_cfg()

func _on_nickname_text_changed(new_text):
	state.cfg["nickname"] = new_text
	write_cfg()