extends "res://assets/scripts/net_constants.gd"

const CONFIG_PATH = "user://dno.cfg"

var port
var ip

var timer = 0.0

var connection
var peer
var enet_unreliable
var connected = false

var timeout = 5

var respawn = true
var respawn_timer = 3.0
var cells = []
var team_queue = []
var in_game = []

var team_points = [0, 0, 0, 0]

var prev_game_timer = 10
var prev_players_wait_timer = 10
var game_timer = MATCH_TIME
var game_timer_stop = true
var players_wait_timer = WAIT_TIME
var timer_timeout = 1.1

var alive_timer = 0.0

var stats = [0, 0, 0]

var hitbox = {
	"lt" : 10,
	"rt" : 10,
	"lb" : 10,
	"rb" : 10,
	"c" : 5
}

var points = [
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	},
	{
		"progress" : 0.0,
		"state" : 1,
		"cap_team" : 0,
		"capturers" : 0,
	}
]

var team = -1

var ui_busy = false

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	state.resize()
	set_process(false)
	randomize()

	$"fg/on_connect_splash/panel/info".text = "Connecting to server..."
	$"fg/on_connect_splash".show()
	$"fg/hud".hide()

	ip = networkv2.ip
	port = networkv2.port
	
	connection = StreamPeerTCP.new()
	enet_unreliable = NetworkedMultiplayerENet.new()
	
	connection.connect_to_host(ip, port)
	if enet_unreliable.create_client(ip, port) != 0:
		print("Enet fail!")
		exit()
		return
	get_tree().set_network_peer(enet_unreliable)
	
	networkv2.info["id"] = enet_unreliable.get_unique_id()

	get_tree().multiplayer.connect("network_peer_packet", self, "unreliable_packet")

	peer = PacketPeerStream.new()
	peer.set_stream_peer(connection)
	if connection.get_status() == connection.STATUS_CONNECTED:
		print("Connected to " + ip + ":" + str(port))
		set_process(true)
		connected = true
		peer.put_var([PLAYER_CONNECT, networkv2.info])
		$"fg/on_connect_splash/panel/info".text = "Connected to server!"
	elif connection.get_status() == StreamPeerTCP.STATUS_CONNECTING:
		print("Trying to connect " + ip + ":" + str(port))
		print("Timeout in: ", timeout, " seconds")
		set_process(true)
	elif connection.get_status() == connection.STATUS_NONE or connection.get_status() == StreamPeerTCP.STATUS_ERROR:
		print("Couldn't connect to " + ip + ":" + str(port))
		$"fg/on_connect_splash/panel/info".text = "Couldn't connect to server!"
		exit()


func _process(delta):
	if !connected:
		
		if connection.get_status() == connection.STATUS_CONNECTED:
			print("Connected to " + ip + ":" + str(port))
			connected = true
			peer.put_var([PLAYER_CONNECT, networkv2.info])
			$"fg/on_connect_splash/panel/info".text = "Connected to server!"
			return
		if timeout > 0:
			timeout -= delta
		else:
			print("Server timed out")
			$"fg/on_connect_splash/panel/info".text = "Couldn't connect to server!"
			exit()
			return
		
	if connection.get_status() == connection.STATUS_NONE or connection.get_status() == connection.STATUS_ERROR:
		print("Server disconnected")
		set_process(false)
		exit()
	
	if alive_timer < ALIVE_TIMER_RATE:
		alive_timer+=delta
		if alive_timer > ALIVE_TIMER_RATE:
			$"/root/game".peer.put_var([ALIVE])
			alive_timer = 0.0
	
	if peer.get_available_packet_count() > 0:
		for i in range(peer.get_available_packet_count()):
			var data = peer.get_var()
			
			match data[0]:
				PLAYER_CONNECT:
					match data[1]:
						0:
							data.remove(0)
							data.remove(0)
							networkv2.players_info[data[0]] = data[1]
							networkv2.players[data[0]] = DEFAULT_PLAYER.duplicate(true)
							
							team_queue.append(data[0])
							
						1:
							$"fg/on_connect_splash/panel/info".text = "Receiving players info..."
							data.remove(0)
							data.remove(0)
							for info in data:
								networkv2.players_info[info[0]] = info[1]
					
				PLAYER_DISCONNECT:
					
					$fg/hud.append_feed("join", [data[1], false])
					
					if in_game.has(data[1]): 
						in_game.erase(data[1])
					networkv2.players.erase(data[1])
					$"fg/hud/minimap/drawer".remove_player(data[1])
					$"fg/tab_menu".update_tab_menu()
					var player_node = has_node("players/" + str(data[1])) 
					if player_node: get_node("players/" + str(data[1])).queue_free()
					
				PLAYER_DATA:
					
					match data[1]:
						"init":
							$"fg/on_connect_splash/panel/info".text = "Receiving players data..."
							data.remove(0)
							data.remove(0)
							for player in data:
								networkv2.players[player[0]] = player[1].duplicate(true)
								if player[1]["playing"]:
									respawn(player[0])
							
						"reset":
							
							var player_node = has_node("players/" + str(data[2]))
							if networkv2.players.has(data[2]):
								networkv2.players[data[2]] = data[3].duplicate(true)
							else:
								hitbox = data[3]["hitbox"].duplicate()
								if player_node: get_node("players/" + str(data[2]) + "/weapon").reset_ammo()
							if player_node:
								for i in data[3]["hitbox"].duplicate().keys():
									get_node("players/" + str(data[2])).visual_damage(data[3]["hitbox"].duplicate()[i], i)
							
							
						"hitbox":
							
							if int(data[2]) == networkv2.info["id"]:
								hitbox[data[3]] = data[4]
								if data[3] == "c":
									if hitbox[data[3]] < 1:
										peer.put_var([PLAYER_DATA, "умер мужик",data[5]])
										$fg/ui.show()
										$fg/hud.hide()
										$fg/hud.hide()
										for j in $bg.get_children():
											j.hide()
										respawn = true
										$camera.respawn = true
										var player_node = has_node("players/" + str(networkv2.info["id"]))
										if player_node: get_node("players/" + str(networkv2.info["id"])).queue_free()
							elif networkv2.players.has(int(data[2])):
								networkv2.players[int(data[2])]["hitbox"][data[3]] = data[4]
							var player_node = has_node("players/" + data[2])
							if player_node: get_node("players/" + data[2]).visual_damage(data[4], data[3])
							
						"hitbox_reset":
							
							if int(data[2]) == networkv2.info["id"]:
								hitbox = data[3].duplicate()
							else:
								networkv2.players[int(data[2])]["hitbox"] = data[3].duplicate()
							var player_node = has_node("players/" + data[2])
							if player_node:
								for i in data[3].duplicate().keys():
									get_node("players/" + data[2]).visual_damage(data[3].duplicate()[i], i)
							
						"умер мужик":
							
							$"fg/hud/minimap/drawer".remove_player(data[2])
							var player_node = has_node("players/" + str(data[2]))
							if player_node: get_node("players/" + str(data[2])).queue_free()
							
						"spawn_pos":
							
							var player_node = has_node("/root/game/players/" + str(networkv2.info["id"]))
							if player_node: get_node("/root/game/players/" + str(networkv2.info["id"])).position = data[2]
							
						"team":
							if data[2] == networkv2.info["id"]:
								team = data[3]
							else:
								networkv2.players[data[2]]["team"] = data[3]
								
								if data[2] in team_queue:
									$"fg/hud".append_feed("join", [data[2],true])
									team_queue.erase(data[2])
							
						"respawn_timer":
							respawn_timer = data[2]
							$"fg/ui/upper_panel/timer".text = "u can respown in " + str(int(respawn_timer + 1)) + " seconds!"
							
						"respawn":
							$"fg/ui/upper_panel/timer".text = "press button to respown1"
							$"fg/ui/upper_panel/respawn".disabled = false
							
						_:
							
							if networkv2.players.has(data[3]):
								networkv2.players[data[3]][data[1]] = data[2]
								
								if data[1] == "playing" and data[2]:
									respawn(data[3])
								
							else:
								print("Wrong packet:\n",data)
						
				MESSAGE:
					
					var text = data[1]
					var id = data[2]
					var team = data[3]
					
					$"fg/chat".update_chat(text, id, team)
					
				ID: networkv2.id = data[1]
				
				MAP:
					
					$"fg/on_connect_splash/panel/info".text = "Receiving map..."
					match data[1]:
						"map":
							for cell in data[2]:
								cells.append(cell)
						"info":
							$tilemap.clear()
							for j in $points.get_children():
								j.queue_free()
							$tilemap.key_points = data[2]
							$tilemap.height = data[3]
							print("received map!")
							$tilemap.load_map(cells)
							var point_counter = 0
							for j in $tilemap.key_points:
								var p = preload("res://assets/scenes/key_point.tscn").instance()
								p.name = "key_point" + str(point_counter)
								p.point_idx = point_counter
								p.position = $tilemap.map_to_world(j)
								$points.add_child(p)
								point_counter += 1
							cells.clear()
							
				POINT:
					
					if data[1] == 42:
						$"fg/on_connect_splash".hide()
						points = data[2]
						
						for i in range(points.size()):
							if has_node("/root/game/fg/hud/minimap/drawer/"+str(i)):
								get_node("/root/game/fg/hud/minimap/drawer/"+str(i)).state = points[i]["state"]
								get_node("/root/game/fg/hud/minimap/drawer/"+str(i)).u()
							$"/root/game/fg/hud".update_points_indicator(i,points[i]["state"])
							
					elif data[1] < points.size():
						points[data[1]][data[2]] = data[3]
						
						if data[2] == "state":
							if has_node("/root/game/fg/hud/minimap/drawer/"+str(data[1])):
								get_node("/root/game/fg/hud/minimap/drawer/"+str(data[1])).state = data[3]
								get_node("/root/game/fg/hud/minimap/drawer/"+str(data[1])).u()
						
					else:
						print("Wrong packet:\n", data)
				
				FEED:
					
					match data[1]:
						"kill": 
							$"fg/hud".append_feed("kill",data[2])
							if in_game.has(data[2][1]): 
								in_game.erase(data[2][1])
								$"fg/tab_menu".update_tab_menu()
							
						"cap": $"fg/hud".append_feed("cap", data[2])
						
						"res":
							
							var who = data[2]
							if !in_game.has(who): 
								in_game.append(who)
								$"fg/tab_menu".update_tab_menu()
							
						"win":
							
							var team = data[2]
							
				TEAM_POINTS:
					
					team_points = data[1]
					$"fg/hud".update_team_points(data[1], data[2])
					
				TIMER:
					
					match data[1]:
						"time":
							game_timer = float(data[2])
						"stop":
							game_timer_stop = data[2]
						"wait":
							players_wait_timer = data[2]
				
				STATS:
					
					var id = data[1]
					var val = data[2]
					
					if id == -1:
						for stat in val:
							if stat[0] == networkv2.info["id"]: stats = stat[1]
							else: networkv2.players[stat[0]]["stats"] = stat[1]
					else:
						if id == networkv2.info["id"]: stats = val
						else: networkv2.players[id]["stats"] = val
					
					$"fg/tab_menu".update_tab_menu()
	
	if timer_timeout > 0:
		timer_timeout -= delta
		if timer_timeout < 0:
			if int(game_timer) > 0:
				$"fg/hud/upper_panel/timer".text = "Waiting for players..."
			else:
				$"fg/hud/upper_panel/timer".text = "Waiting for win move!"
	
	if game_timer > 0 and !game_timer_stop:
		game_timer-=delta
	
	if players_wait_timer > 1:
		if prev_players_wait_timer != int(players_wait_timer):
			$"fg/hud".update_timer(int(players_wait_timer), true)
			prev_players_wait_timer = int(players_wait_timer)
			timer_timeout = 1.1
	
	if prev_game_timer != int(game_timer) and !game_timer_stop:
		$"fg/hud".update_timer(int(game_timer))
		prev_game_timer = int(game_timer)
		timer_timeout = 1.5
	
func unreliable_packet(id,raw_data):
	
	var data = bytes2var(raw_data)
	if data[0] == networkv2.info["id"]: return

	if data[1] == "hitbox":
		networkv2.players[data[0]]["hitbox"] = data[2].duplicate()

	if networkv2.players.has(data[0]):
	
		if networkv2.players[data[0]].has(data[1]):
			networkv2.players[data[0]][data[1]] = data[2]
		else:
			print("Wrong packet key:\n",data)
	else:
		print("Wrong packet:\n",data)
	
func respawn(id=-1):
	var pre_p = preload("res://assets/scenes/mech.tscn")
	var p = pre_p.instance()
	
	if id == -1:
		$"fg/ui".hide()
		$"fg/hud".show()
		for j in $bg.get_children():
			j.show()
		respawn = false
		$camera.respawn = false
		p.name = str(networkv2.info["id"])
		peer.put_var([PLAYER_DATA, "playing", true])
	else:
		p.name = str(id)
	
	p.position = Vector2(0, -5000)
	
	$players.add_child(p)
	
func exit():
	set_process(false)
	if connection:
		connection.disconnect_from_host()
		get_tree().set_network_peer(null)
		connection = false
		
	yield(get_tree().create_timer(0.1), "timeout")
	
	get_tree().change_scene("res://assets/scenes/main.tscn")

func _on_respawn_pressed():
	respawn()
	$"fg/ui/upper_panel/respawn".disabled = true

func fullscreen_update():
	state.fullscreen = state.cfg["fullscreen"]
	OS.window_fullscreen = state.cfg["fullscreen"]
	state.resize()
	state.cfg["fullscreen"] = state.cfg["fullscreen"]
	write_cfg()

func sound_update():
	state.sound = state.cfg["sound"]
	state.update_sound()
	state.cfg["sound"] = state.cfg["sound"]
	write_cfg()

func write_cfg():
	var c = File.new()
	
	if c.open(CONFIG_PATH,File.WRITE) != OK:
		print("Can't write to config file!")
		c.close()
	else:
		c.store_string(to_json(state.cfg))
		c.close()

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		$"/root/game".peer.put_var([13])