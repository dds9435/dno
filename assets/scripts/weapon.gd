extends Node2D

var orientation = false
var weapons = {
	"01" : {
		"textures" : {
			"base" : "res://assets/graphics/weapons/gun01.png",
			"bolt" : "res://assets/graphics/weapons/bolt01.png"
		},
		"transform" : {
			"base_offset" : Vector2(28,2),
			"bolt_pos" : Vector2(21.5,-0.5),
			"bolt_out_pos" : Vector2(9.5,-0.5),
			"out_pos" : Vector2(44,-0.5),
			"recoil_degrees_difference" : 10,
			"recoil_delta" : 0.2,
			"recoil_return_delta" : 0.2,
			"animation_type" : "firearm"
		},
		"prop" : {
			"bullet_type" : "firearm",
			"recoil" : 0.5,
			"reload_rate" : 0.7,
			"ammo" : 15, 
			"damage" : 3
		}
	},
	"02" : {
		"textures" : {
			"base" : "res://assets/graphics/weapons/gun02.png",
			"bolt" : "res://assets/graphics/weapons/bolt02.png"
		},
		"transform" : {
			"base_offset" : Vector2(32,2),
			"bolt_pos" : Vector2(20,0),
			"bolt_out_pos" : Vector2(5,0),
			"out_pos" : Vector2(54,0),
			"recoil_degrees_difference" : 12,
			"recoil_delta" : 0.4,
			"recoil_return_delta" : 0.2,
			"animation_type" : "firearm"
		},
		"prop" : {
			"bullet_type" : "firearm",
			"recoil" : 0.15,
			"reload_rate" : 0.25,
			"ammo" : 30,
			"damage" : 5
		}
	},
	"03" : {
		"textures" : {
			"base" : "res://assets/graphics/weapons/gun03.png",
		},
		"transform" : {
			"base_offset" : Vector2(36,2),
			"out_pos" : Vector2(57.5,0.5),
			"recoil_degrees_difference" : 15,
			"recoil_delta" : 0.4,
			"recoil_return_delta" : 0.6,
			"animation_type" : "shocker"
		},
		"prop" : {
			"bullet_type" : "shocker",
			"recoil" : 1.2,
			"reload_rate" : 1.5,
			"ammo" : 5,
			"damage" : 3
		}
	},
	"04" : {
		"textures" : {
			"base" : "res://assets/graphics/weapons/gun04.png",
			"bolt" : "res://assets/graphics/weapons/bolt04.png"
		},
		"transform" : {
			"base_offset" : Vector2(32,2),
			"bolt_pos" : Vector2(21.5,0),
			"bolt_out_pos" : Vector2(-5.5,0),
			"out_pos" : Vector2(53,0),
			"recoil_degrees_difference" : 15,
			"recoil_delta" : 0.4,
			"recoil_return_delta" : 0.6,
			"animation_type" : "firearm"
		},
		"prop" : {
			"bullet_type" : "grenade",
			"recoil" : 0.8,
			"reload_rate" : 0.5,
			"ammo" : 10,
			"damage" : 3
		}
	},
	"05" : {
		"textures" : {
			"base" : "res://assets/graphics/weapons/gun05.png",
			"bolt" : "res://assets/graphics/weapons/bolt05.png"
		},
		"transform" : {
			"base_offset" : Vector2(36,3.5),
			"bolt_pos" : Vector2(17,0),
			"bolt_out_pos" : Vector2(-7,0),
			"out_pos" : Vector2(68,0),
			"recoil_degrees_difference" : 25,
			"recoil_delta" : 0.4,
			"recoil_return_delta" : 0.6,
			"animation_type" : "firearm"
		},
		"prop" : {
			"bullet_type" : "snipe",
			"recoil" : 1.0,
			"reload_rate" : 0.8,
			"ammo" : 5,
			"damage" : 3
		}
	}
}

var prev_ammo = 0

var weapon_id = null

var ammo = 0
var recoil_timer = 0.0
var reload_timer = 0.0
var return_angle = 0.0

var bolt_pos = Vector2(0,0)
var bolt_out_pos = Vector2(0,0)
var recoil_degrees_difference = 0
var recoil_delta = 0.0
var recoil_return_delta = 0.0
var animation_type = ""

var prop_bullet_type = ""
var prop_ammo = 0
var prop_recoil = 0
var prop_reload_rate = 0.0
var prop_damage = 0

var sound_set = {
	"01" : [preload("res://assets/sounds/PISTOL1.ogg"),preload("res://assets/sounds/PISTOL2.ogg"),preload("res://assets/sounds/PISTOL3.ogg"),preload("res://assets/sounds/ENEMYPISTOL1.ogg"),preload("res://assets/sounds/ENEMYPISTOL2.ogg"),preload("res://assets/sounds/ENEMYPISTOL3.ogg")],
	"02" : [preload("res://assets/sounds/RIFLE1.ogg"),preload("res://assets/sounds/RIFLE2.ogg"),preload("res://assets/sounds/RIFLE3.ogg"),preload("res://assets/sounds/ENEMYRIFLE1.ogg"),preload("res://assets/sounds/ENEMYRIFLE2.ogg"),preload("res://assets/sounds/ENEMYRIFLE3.ogg")]
}

var prev_util_rot = 0.0

func _ready():
	randomize()

func shoot():
	$util/out/fire.animation = str(randi()%3)
	$util/out/fire.frame = 0
	
	match prop_bullet_type:
		"firearm":
			var p = preload("res://assets/scenes/bullet/firearm_bullet.tscn").instance()
			
			var col_point = $util/out.global_position+Vector2(16384,0).rotated(rotation)
			var target = ""
			var target_part = ""
			
			if $util/ray.is_colliding():
				col_point = $util/ray.get_collision_point()
				target = $util/ray.get_collider().get_node("../..").name
				target_part = $util/ray.get_collider().name
			
			p.points[0] = $util/out.global_position
			p.points[1] = col_point
			
			$"/root/game/proj".add_child(p)
			
			if networkv2.players.keys().has(int(target)):
				if $"..".name == str(networkv2.info["id"]) and networkv2.players[int(target)]["team"] != $"/root/game".team:
					networkv2.send_hit("firearm",$"..".name,target,target_part,col_point,prop_damage)
			
	
	if !sound_set.has(weapon_id): return
	if has_node("/root/game/camera"):
		if $"/root/game/camera".get_camera_position().distance_to(global_position) > 3500:
			$sound.stream = sound_set[weapon_id][randi()%3+3]
		else:
			$sound.stream = sound_set[weapon_id][randi()%3]
	else:
		$sound.stream = sound_set[weapon_id][randi()%3]
	$sound.stop()
	$sound.play(0)
	
	# animation
	match animation_type:
		"firearm":
			$bolt_tw.start()
			$bolt_tw.interpolate_property($base/bolt,"position",bolt_pos,bolt_out_pos,0.05,Tween.TRANS_QUAD,Tween.EASE_IN_OUT)
	
	$rot_tw.start()
	$rot_tw.interpolate_property($base,"rotation_degrees",recoil_degrees_difference*(-1+2*int(orientation)),return_angle*(-1+2*int(orientation)),prop_recoil,Tween.TRANS_SINE,Tween.EASE_IN_OUT)
		
	# animation
	match animation_type:
		"firearm":
			yield($bolt_tw,"tween_completed")
			$bolt_tw.interpolate_property($base/bolt,"position",bolt_out_pos,bolt_pos,0.2,Tween.TRANS_QUAD,Tween.EASE_IN_OUT)

func reset_ammo():
	ammo = prop_ammo

func init_weapon(id):
	if id in weapons.keys():
		weapon_id = id
		$base.texture = load(weapons[id]["textures"]["base"])
		if weapons[id]["textures"].has("bolt"): $base/bolt.texture = load(weapons[id]["textures"]["bolt"])
		$base.offset = weapons[id]["transform"]["base_offset"]
		if weapons[id]["transform"].has("bolt_pos"): $base/bolt.position = weapons[id]["transform"]["bolt_pos"]
		$util/out.position = weapons[id]["transform"]["out_pos"]
		$util/ray.position = weapons[id]["transform"]["out_pos"]
		if weapons[id]["transform"].has("bolt_pos"): bolt_pos = weapons[id]["transform"]["bolt_pos"]
		if weapons[id]["transform"].has("bolt_out_pos"): bolt_out_pos = weapons[id]["transform"]["bolt_out_pos"]
		recoil_degrees_difference = weapons[id]["transform"]["recoil_degrees_difference"]
		recoil_delta = weapons[id]["transform"]["recoil_delta"]
		recoil_return_delta = weapons[id]["transform"]["recoil_return_delta"]
		animation_type = weapons[id]["transform"]["animation_type"]
		prop_bullet_type = weapons[id]["prop"]["bullet_type"]
		prop_ammo = weapons[id]["prop"]["ammo"]
		prop_recoil = weapons[id]["prop"]["recoil"]
		prop_reload_rate = weapons[id]["prop"]["reload_rate"]
		prop_damage = weapons[id]["prop"]["damage"]
		ammo = prop_ammo
	else:
		print("Noexisting weapon id "+id+"!")

func _process(delta):
	
	if $util/out/fire.frame == 0:
		$util/out/light.enabled = true
	else:
		$util/out/light.enabled = false
	
	var shoot = Input.is_action_pressed("shoot")
	
	if $"..".slaved and networkv2.players.has(int($"..".name)):
		$util.rotation_degrees = networkv2.players[int($"..".name)]["util_rot"]
		$base.rotation_degrees = networkv2.players[int($"..".name)]["util_rot"]
		if networkv2.players[int($"..".name)]["shoot"]:
			networkv2.players[int($"..".name)]["shoot"] = false
			shoot()
	
	if !$"..".slaved:
		
		if !recoil_timer > 0.0 and !shoot:
			reload_timer+=delta
			if !reload_timer < prop_reload_rate:
				reload_timer = 0.0
				if ammo < prop_ammo:
					ammo+=1
		
		if recoil_timer > 0.0:
			recoil_timer -= delta
		
		if !recoil_timer > 0.0 or (return_angle - recoil_degrees_difference) > -2.5:  
			if return_angle > 0:
				return_angle-=recoil_return_delta
				$base.rotation_degrees = return_angle*(-1+2*int(orientation))
				$util.rotation_degrees = return_angle*(-1+2*int(orientation))
				if prev_util_rot != $util.rotation_degrees:
					networkv2.send_data("util_rot",$util.rotation_degrees)
					prev_util_rot = $util.rotation_degrees
		
		if !$"/root/game".ui_busy and shoot and !recoil_timer > 0.0 and ammo > 0:
			recoil_timer = prop_recoil
			networkv2.send_data("shoot",true)
			shoot()
			ammo-=1
			if return_angle < recoil_degrees_difference:
				return_angle+=recoil_delta*(1-randf())*2
		
		if prev_ammo != ammo:
			$"/root/game/fg/dot".update_bar(float(ammo)/prop_ammo*100)
			prev_ammo = ammo
	
	if orientation: 
		$base.flip_v = true
		$base.offset.y = -abs($base.offset.y)
		$base/bolt.flip_v = true
		$base/bolt.position.y = abs($base/bolt.position.y)
		$util/out.position.y = abs($util/out.position.y)
		$util/ray.position.y = abs($util/ray.position.y)
		
	else: 
		$base.flip_v = false
		$base.offset.y = abs($base.offset.y)
		$base/bolt.flip_v = false
		$base/bolt.position.y = -abs($base/bolt.position.y)
		$util/out.position.y = -abs($util/out.position.y)
		$util/ray.position.y = -abs($util/ray.position.y)
		
		