shader_type canvas_item;

uniform sampler2D noise;
uniform float noise_res = 1.0;
uniform float force = 0.0;

void fragment(){
	vec4 tex = texture(TEXTURE,UV);
	vec4 n = texture(noise,UV*noise_res);
	
	if (n.r < force) {
		
		COLOR=vec4(0.0);
		
	}
	
	else {
		COLOR = tex;
	}
	
}