extends Control

var zoom = 1
var pressed = false
var active = false

var out = {}

func save():
	var f = File.new()
	f.open($base/folder.text+"/polygons.db",File.WRITE)
	f.store_var(out)
	f.close()

func _on_view_gui_input(event):
	if !active: return
	if event.get_class() == "InputEventMouseButton": 
		if event.button_index == 1 and event.pressed:
			pressed = true
			if !$base/container/view.points.has((event.position.snapped(Vector2(zoom,zoom))/zoom)):
				$base/container/view.points.append((event.position.snapped(Vector2(zoom,zoom))/zoom))
				$base/container/view.update()
				out[$base/container/view.texture.resource_path] = $base/container/view.points
				save()
		else:
			pressed = false

		if event.button_index == 2 and event.pressed:
			if $base/container/view.highlight_point != null:
				$base/container/view.points.erase($base/container/view.highlight_point)
				$base/container/view.highlight_point = null
				$base/container/view.update()
				save()
		
	if event.get_class() == "InputEventMouseMotion":
		if $base/container/view.mouse_position != event.position.snapped(Vector2(zoom,zoom)):
			$base/container/view.mouse_position = event.position.snapped(Vector2(zoom,zoom))
			$base/container/view.update()
		for i in $base/container/view.points:
			if event.position.distance_to(i*zoom) < 16 and !pressed:
				$base/container/view.highlight_point = i
				$base/container/view.update()
		if $base/container/view.highlight_point != null:
			if event.position.distance_to($base/container/view.highlight_point*zoom) > 16 and !pressed:
				$base/container/view.highlight_point = null
				$base/container/view.update()
			elif pressed and Rect2(Vector2(0,0),$base/container/view.rect_size).has_point(event.position):
				var idx = $base/container/view.points.find($base/container/view.highlight_point)
				$base/container/view.points[idx] = event.position.snapped(Vector2(zoom,zoom))/zoom
				$base/container/view.highlight_point = event.position.snapped(Vector2(zoom,zoom))/zoom
				$base/container/view.update()
				save()



func _on_container_gui_input(event):
	if !active: return
	if event.get_class() == "InputEventMouseButton": 
		if event.button_index == 4 and event.pressed:
			if $base/container/view.texture.get_size() * (zoom) < $base/container.rect_size:
				zoom+=0.5
				$base/container/view.rect_size = $base/container/view.texture.get_size() * zoom
				$base/container/view.rect_position = -$base/container/view.rect_size/2 + $base/container.rect_size/2
				$base/container/polygon.position = -$base/container/view.rect_size/2 + $base/container.rect_size/2
		if event.button_index == 5 and event.pressed:
			if zoom-1 != 0:
				zoom-=0.5
				$base/container/view.rect_size = $base/container/view.texture.get_size() * zoom
				$base/container/view.rect_position = -$base/container/view.rect_size/2 + $base/container.rect_size/2
				$base/container/polygon.position = -$base/container/view.rect_size/2 + $base/container.rect_size/2

func _on_folder_b_pressed():
	$dialogs/textures_folder.popup()

func _on_textures_folder_dir_selected(dir):
	$base/folder.text = dir
	dir_contents(dir)

func dir_contents(path):
	$base/list.clear()
	var dir = Directory.new()
	if dir.open(path) == OK:
		if dir.file_exists(path+"/polygons.db"):
			var f = File.new()
			f.open(path+"/polygons.db",File.READ)
			out = f.get_var()
			f.close()
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while (file_name != ""):
			if !dir.current_is_dir() and file_name.ends_with(".png"):
				$base/list.add_item(file_name)
			file_name = dir.get_next()
	else:
		print("An error occurred when trying to access the path.")


func _on_list_item_selected(index):
	$base/container/view.texture = load($base/folder.text+"/"+$base/list.get_item_text(index))
	$base/container/view.rect_size = $base/container/view.texture.get_size() * zoom
	$base/container/view.rect_position = -$base/container/view.rect_size/2 + $base/container.rect_size/2
	$base/container/polygon.position = -$base/container/view.rect_size/2 + $base/container.rect_size/2
	$base/container/polygon.polygon = []
	$base/container/view.points = []
	if out.has($base/container/view.texture.resource_path):
		$base/container/polygon.polygon = out[$base/container/view.texture.resource_path]
		$base/container/view.points = out[$base/container/view.texture.resource_path]
	active = true
	$base/container/view.update()


func _on_list_nothing_selected():
	active = false
