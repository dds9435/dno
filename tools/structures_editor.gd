tool

extends Control

export var Struct_name = ""
export var Save = false
export var Erase = false
var old_struct = ""

var data = {}

func _ready():
	var f = File.new()
	if f.open("res://assets/structures.db",File.READ) == 0:
		data = f.get_var()
		f.close()
	set_process(true)

func _process(delta):
	if Struct_name != old_struct:
		print("Trying to load...")
		if data.has(Struct_name):
			$tilemap.clear()
			for i in data[Struct_name]["tilemap"].keys():
				for j in data[Struct_name]["tilemap"][i]:
					$tilemap.set_cell(int(j.x),int(j.y),int(i))
			print("Loaded!")
		print("Available structures: ")
		for i in data.keys():
			print(i)
		old_struct = Struct_name
	if Save:
		print("Saving!")
		data[Struct_name] = {"tilemap" : {}}
		for i in $tilemap.tile_set.get_tiles_ids():
			data[Struct_name]["tilemap"][i] = $tilemap.get_used_cells_by_id(i)
		var f = File.new()
		f.open("res://assets/structures.db",File.WRITE)
		f.store_var(data)
		f.close()
		Save = false
	if Erase:
		$tilemap.clear()
		$tilemap/btilemap.clear()
		if data.has(Struct_name):
			data.erase(Struct_name)
			var f = File.new()
			f.open("res://assets/structures.db",File.WRITE)
			f.store_var(data)
			f.close()
		Erase = false